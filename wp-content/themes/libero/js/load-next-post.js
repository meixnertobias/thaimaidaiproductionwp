jQuery(window).load(function($){
	$ = jQuery;
	$(window).scroll(function(){				
		// Ajax Action
		if( $('.libero-loadnextpost').length ) {
			var _this = $('.libero-loadnextpost');
			if ( $(window).scrollTop() > ( _this.offset().top - $(window).height()) ){
				_this.removeClass('hidden');
				if( _this.attr('data-disable') == undefined ) _this.attr('data-disable',0);
				_this.attr('data-disable', _this.attr('data-disable')*1+1 );
				
				if( $('#libero-next-post').attr('data-id') != '0' && parseFloat(_this.attr('data-disable')) == 1 ) {
					$('.libero-loadnextpost').addClass('active');
					$.ajax({
						type: "POST",
						url: rs.wordpress.home_url ,
						dataType: 'json',
						data: {
							'p'		: $('#libero-next-post').attr('data-id'),
							'libero_next_post'		: '1'
						},
						success: function(data){
							_this.attr('data-disable', 0 );
							$('.libero-loadnextpost').removeClass('active');
							$('#libero-next-post').attr('data-id',data.id);
							$(".libero-iframe").fitVids();
							if( data.id == 0 ) {
								$('.libero-loadnextpost').remove();
							}
							$( '<div class="libero-ajax-content post-detail-row">' + data.html + '</div>').appendTo( $('#libero-next-post') ).hide().slideDown(300,function(){
								$(window).trigger('scroll');
							}).find("#sidebar").stickit({
								scope: StickScope.Parent,
								top: 0
							});

							jQuery(".libero_recent_post_owl").owlCarousel({
								items: 1,
								nav : true,
								loop: true,
								navText : ['<i class="fa fa-angle-double-left"></i>','<i class="fa fa-angle-double-right"></i>'],
								dots : false,
							});
						}
					});
				}
			}
		}
		//Progress
		$('#liber-content .post-detail-row').each(function(){
			var scrollTop = $(window).scrollTop();
			var heightWindow = $(window).height();
			var headerStickyHeight = $('.libero_header_sticky').height();
			var singleTop = $(this).offset().top;
			var heightSingle = $(this).outerHeight(true);
			var singleBottom = heightSingle + singleTop;
			
			if( scrollTop >= singleTop && scrollTop <= singleBottom ) {
				var singleTitleHTML = $(this).find('h1.libero_detail_title').html();
				var singleTitle = $(this).find('h1.libero_detail_title').text();
				var singleLink = $(this).find('.libero-hidden-link').html();
				if( singleLink != '' ) {
					window.history.pushState({path: singleLink},'',singleLink);
				}
				$('.libero_sticky_post_title .libero-post-title').html(singleTitleHTML);
				document.title = singleTitle;
				var progress = ( scrollTop - singleTop ) / heightSingle * 100;
				$('.libero-progress').css('width',progress + '%');
			}
		});
	});
});