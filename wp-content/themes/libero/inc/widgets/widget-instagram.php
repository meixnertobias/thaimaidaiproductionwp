<?php
class libero_instagram_widget extends WP_Widget {
	
	function libero_instagram_widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget-instagram', 'description' => 'A widget that show images instagram account' );
		/* Create the widget. */
		parent::__construct( 'libero-instagram-widget', 'Libero - Instagram', $widget_ops);	
	}
	 
	function widget($args, $instance) {
		$html = '';
		$html .= $args['before_widget'];
		if( !empty($instance['title']) ) {
			$html .= $args['before_title'];
				$html .= $instance['title'];
			$html .= $args['after_title'];
		}
		$html .= '<div class="widget-wrap">';
			$html .= '<ul class="clearfix">';
		
				$userID = isset($instance['user_id']) ? $instance['user_id'] : '';
				$count = isset($instance['count']) ? absint($instance['count']) : 9;
				if($userID && $userID != ''){
					$results_array = libero_instagram_widget::scrape_insta($userID);
					if( $results_array ) {
						$images  = $results_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
						$i = 0; 
						foreach ($images as $image): 
							if($count == $i){ break;}
							$i++;
							$image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
							$link = 'https://instagram.com/p/' . $image['code'];
							$html .= '<li><a target="_blank" href="'. $link .'">';
								$html .= '<img class="img-responsive" alt="" src="'.$image['thumbnail_src'] .'" />';
							$html .= '</a></li>';
						endforeach;
					}
				}
				
				
			$html .= '</ul>';
		$html .= '</div>';
		
		$html .= $args['after_widget'];
		echo force_balance_tags($html);
	}
	
	function scrape_insta($username) {
		$username = strtolower( $username );
		$username = str_replace( '@', '', $username );
		$remote = wp_remote_get( 'http://instagram.com/'.trim( $username ) );
		$shards = explode( 'window._sharedData = ', $remote['body'] );
		$insta_json = explode( ';</script>', $shards[1] );
		$insta_array = json_decode( $insta_json[0], TRUE );
		//var_dump($insta_array);
		return $insta_array;
	}
 
	function update($new_instance, $old_instance) {
		return $new_instance;		
	}
 
	function form($instance) {
		?><br/>
		<label><?php esc_html_e('Title','libero') ?>:
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo isset($instance['title']) ? esc_attr($instance['title']) : ''; ?>" style="width:100%" /></label>
		<br /><br />
		<label><?php esc_html_e('User Name','libero') ?>:<input type="text" id="<?php echo esc_attr($this->get_field_id( 'user_id' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'user_id' )); ?>" value="<?php echo isset($instance['user_id']) ? esc_attr($instance['user_id']) : ''; ?>" style="width:100%" /></label>
		<br /><br />
		
		<label><?php esc_html_e('Number Photo','libero') ?>:
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'count' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'count' )); ?>" value="<?php echo isset($instance['count']) ? esc_attr($instance['count']) : '9'; ?>" style="width:100%" /></label>
		
		<?php	
	}
}
add_action( 'widgets_init', 'libero_create_instagram_widget' );
function libero_create_instagram_widget(){
	return register_widget("libero_instagram_widget");
}