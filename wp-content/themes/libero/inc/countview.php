<?php
/*
 * Function Count View
 */
function libero_set_post_views($postID) {
	$count_key = 'libero_post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '1');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}
function libero_track_post_views ($post_id) {
	if ( !is_single() && !is_page() ) return;
	if ( empty ( $post_id) ) {
		global $post;
		$post_id = $post->ID;    
	}
	libero_set_post_views($post_id);
}

//To keep the count accurate, lets get rid of prefetching
add_action( 'wp_head', 'libero_track_post_views');

function libero_get_views($postID){
	$count_key = 'libero_post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count=='' || $count <= 1 ){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '1');
		return 1;
	}
	return number_format($count);
}