<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package libero
 */

if(rs::isAjax() && isset( $_REQUEST['libero_next_post'] ) ){
	ob_start();
	get_template_part( 'template-parts/single' ); 
	$html = ob_get_clean();
	$postnext = get_next_post();
	$data = array();
	if( $postnext ) {
		$data['id']	= $postnext->ID;
	}
	else {
		$data['id']	= 0;
	}
	$data['html'] = $html;
	echo json_encode($data);
}
else{
	get_header(); ?>

	<div id="primary" class="content-area">
		
		<?php if( !get_theme_mod('disable_loading_content') ) echo '<div class="pageLoading"><div class="pageLoadingInner"></div></div>'; ?>
	
		<div class="container libero_detail">
			
			<?php get_template_part( 'template-parts/single' ); ?>
			
			<?php
				if( get_theme_mod('single_enable_next_post') ) {
					get_template_part( 'template-parts/loadnextpost' );
				}
			?>
		
		</div>
		
	</div><!-- #primary -->

	<?php 
	get_footer(); 
} 
?>
