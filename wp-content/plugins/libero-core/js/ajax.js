jQuery(document).ready(function($){
	
	// Ajax Sub Category Menu 
	$('.libero-wrap-ajax .libero_subcat_ajax').click(function(e){
		e.preventDefault();
		_this = $(this);
		rst_key = _this.attr('data-key');
		rst_key_eval = eval(_this.attr('data-key'));
		rst_term = _this.attr('data-term');
		if( ! $('#'+rst_key+'-'+rst_term).length && rst_key != undefined && rst_term != undefined ) {
			if( _this.attr('data-disable') == undefined ) _this.attr('data-disable',0);
			_this.attr('data-disable',parseFloat(_this.attr('data-disable')+1));
			if( parseFloat(_this.attr('data-disable')) == 1 ){
				_this.parents('.libero-wrap-ajax').find('.sk-cube-grid-wrap').show();
				_this.parents('.breadcrumb').find('.active').removeClass('active');
				_this.parent().addClass('active');
				$.ajax({
					type: "POST",
					url: libero.ajaxurl,
					data: { 
						'action' : 'libero_ajax_horizontal',
						'atts' : rst_key_eval.atts,
						'template' : rst_key_eval.template,
						'term' : rst_term
					}
				}).done(function(data){
					when_images_loaded(jQuery(data), function(){
						_this.parents('.libero-wrap-ajax').find('.tab-ajax').hide();
						_this.parents('.libero-wrap-ajax').find('.libero-inner-ajax').append('<div id="'+rst_key+'-'+rst_term+'" class="tab-ajax">'+data+'</div>').show();
						_this.parents('.libero-wrap-ajax').find('.sk-cube-grid-wrap').hide();
						
						jQuery(".libero_category_style_4").owlCarousel({
							items: 1,
							loop: true,
							nav : true,
							navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
							dots : true,
						});
					});
				});
			}
		}
		else {
			_this.parents('.libero-wrap-ajax').find('.tab-ajax:visible').slideUp(1,function(){
				if( rst_key != undefined && rst_term != undefined ) {
					_this.parents('.libero-wrap-ajax').find('#'+rst_key+'-'+rst_term).fadeIn(400);
				}
				else {
					_this.parents('.libero-wrap-ajax').find('.tab-ajax').eq(0).fadeIn(400);
				}
			});
		}
	});
	
	
	// Click Load more
	$(document).on('click', '.libero_loadmore', function(e){
		e.preventDefault();
		_this = $(this);
		paged = parseFloat(_this.attr('data-paged'));
		max_paged = parseFloat(_this.attr('data-max-paged'));
		rst_key_eval = eval(_this.attr('data-key'));
		_this.attr('data-disable',parseFloat(_this.attr('data-disable')+1));
		if( parseFloat(_this.attr('data-disable')) == 1 ){
			_this.hide();
			_this.parents('.libero-wrap-ajax').find('.sk-cube-grid-wrap').show();
			$.ajax({
				type: "POST",
				url: libero.ajaxurl,
				data: { 
					'action' : 'libero_ajax_blog',
					'atts' : rst_key_eval.atts,
					'template' : rst_key_eval.template,
					'paged' : paged
				}
			}).done(function(data){
				when_images_loaded(jQuery(data), function(){
					setTimeout(function() {
						_this.attr('data-disable', 0 );
						_this.show();
						_this.parents('.libero-wrap-ajax').find('.sk-cube-grid-wrap').hide();
						var items = jQuery('<div class="libero-ajax-content">' + data + '</div>');
						items.appendTo( _this.parents('.libero-wrap-ajax').find('.libero-inner-ajax') ).hide().slideDown(300,function(){
							$(window).trigger('scroll');
						});
						if( paged < max_paged ) {
							_this.attr('data-paged',parseFloat(paged)+1);
						} 
						else {
							_this.remove();
						}
						$(".libero-iframe").fitVids();
					}, 1000);
				});
			});
		}
	});
	
	// Scroll Window Load more
	$(window).scroll(function(event){
		$('.libero_loadmore').each(function(index){
			if ( $(window).scrollTop() >= ($(this).offset().top - $(window).height()) ){
				if( $(this).attr('data-count-ajax') == 0 ) {
					$(this).click().attr('data-count-ajax','1');
				}
			}
		});
	});
});

function when_images_loaded($img_container, callback) { 
	//do callback when images in $img_container are loaded. Only works when ALL images in $img_container are newly inserted images.
	var img_length = $img_container.find('img').length,
		img_load_cntr = 0;

	if (img_length) { //if the $img_container contains new images.
		$img_container.find('img').load(function() { //then we avoid the callback until images are loaded
			img_load_cntr++;
			if (img_load_cntr == img_length) {
				callback();
			}
		});
	}
	else { //otherwise just do the main callback action if there's no images in $img_container.
		callback();
	}
}