<?php
$attributes_container = array(
	"type" => "dropdown",
    "heading" => __('Container', LANGUAGE_ZONE),
    "param_name" => "select_width",
    "value" => array(
		__("Container", LANGUAGE_ZONE) => '2',
		__("Full Width", LANGUAGE_ZONE) => '1',
	),
	"description" => __("Select width of container", LANGUAGE_ZONE),
	'weight' => 1,
);

vc_add_param('vc_row', $attributes_container);


$attributes_widget_sidebar = array(
	"type" => "dropdown",
    "heading" => __('Title Widget Style', LANGUAGE_ZONE),
    "param_name" => "sidebar_style",
    "value" => array(
		__("Default", LANGUAGE_ZONE) => '0',
		__("Center", LANGUAGE_ZONE) => '1',
	),
	'weight' => 1,
);

vc_add_param('vc_widget_sidebar', $attributes_widget_sidebar);

$attributes_widget_sidebar_sticky = array(
	"type" => "checkbox",
    "heading" => __('Sidebar Sticky', LANGUAGE_ZONE),
    "param_name" => "sidebar_is_sticky",
	'weight' => 1,
);

vc_add_param('vc_column', $attributes_widget_sidebar_sticky);
