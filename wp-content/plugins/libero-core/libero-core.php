<?php
/**
 * Plugin Name: Libero Core
 * Description: Plugins Libero Core setup only theme Libero.
 * Version: 1.3
 * License: A "Slug" license name e.g. GPL2
 */

define( 'LANGUAGE_ZONE', 'libero' );

define( 'LIBERO_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

define( 'LIBERO_PLUGIN_URL', untrailingslashit( plugins_url( '', __FILE__ ) ) );


function libero_core_backend_enqueue() {
	wp_enqueue_style( 'libero_core_style', LIBERO_PLUGIN_URL .'/style.css' );
}
add_action( 'admin_enqueue_scripts', 'libero_core_backend_enqueue', 10000 );


function libero_core_front_enqueue() {
	wp_enqueue_script( 'libero_core_front_block_script', LIBERO_PLUGIN_URL .'/js/block.js',array('jquery'), '', false );
	
	wp_enqueue_script( 'libero_core_front_ajax_script', LIBERO_PLUGIN_URL .'/js/ajax.js',array('jquery'), '', true );
	wp_localize_script( 'libero_core_front_ajax_script', 'libero', array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'textloading' => esc_html__('Loading...','libero'),
		'textloadmore' => esc_html__('Load more','libero'),
	));
}
add_action( 'wp_enqueue_scripts', 'libero_core_front_enqueue', 1000 );


/**
 * Shortcode.
 */
require LIBERO_PLUGIN_DIR . '/include/functions.php';
require LIBERO_PLUGIN_DIR . '/include/ajax-action.php';
require LIBERO_PLUGIN_DIR . '/include/shortcode/shortcode.php';

/**
 * Import Data.
 */
require LIBERO_PLUGIN_DIR . '/include/import/rst-import-data-demo.php';

/**
 * Set Featured Images.
 */
require LIBERO_PLUGIN_DIR . '/include/custom-featured-image.php';


function libero_core_init() {
	
	// if( !class_exists('RS') ) return;
	
	/*
	 * Function echo Class
	 */
	if ( ! function_exists( 'libero_class' ) ) {
		function libero_class($args) {
			$class = '';
			if( !is_array($args) ) return $args;
			foreach($args as $item) $class .= trim($item) .' ';
			return trim($class);
		}
	}
	
	/*
	 * Function Get Excerpt
	 */
	if ( ! function_exists( 'libero_get_excerpt_by_id' ) ) {
		function libero_get_excerpt_by_id($post, $length = 0, $tags = '<a><em><strong>', $extra = '...') {
			$old_post = $post;
			if(is_int($post)) {
				$post = get_post($post);
			} elseif(!is_object($post)) {
				return false;
			}
			if( is_object($post) ) setup_postdata($post);
			
			if($length == 0) return apply_filters('the_content', $post->post_content);
			
			// $the_excerpt = apply_filters( 'get_the_excerpt', $post->post_excerpt );
			$the_excerpt = ( empty($post->post_excerpt) ) ? $post->post_content : $post->post_excerpt;
			
			$the_excerpt = strip_shortcodes(strip_tags($the_excerpt), $tags);
			
			$the_excerpt = preg_split('/\b/', $the_excerpt, $length * 2+1);
			$excerpt_waste = array_pop($the_excerpt);
			$the_excerpt = implode($the_excerpt);
			$the_excerpt = trim($the_excerpt);
			$the_excerpt = str_replace("\n","",$the_excerpt);
			$the_excerpt = str_replace("\r","",$the_excerpt);
			
			if( $the_excerpt != '' )
				$the_excerpt .= $extra;
			
			$post = $old_post;
			
			return wpautop($the_excerpt);
		}
	}
	
	if ( class_exists( 'WooCommerce' ) ) {
		include( LIBERO_PLUGIN_DIR.'/include/post-type/brand-taxonomy-product.php');
	}
}
add_action('init','libero_core_init', 999);

function libero_js_composer_init() {
	
	// Visual Composer HOOKS
	function rd_check_vc_status() {
		include_once(ABSPATH.'wp-admin/includes/plugin.php');
		if(is_plugin_active('js_composer/js_composer.php')) {
		 return true;
		}
		else{
			return false;
		}
	}
	
	require LIBERO_PLUGIN_DIR . '/include/shortcode_query.php';
	require LIBERO_PLUGIN_DIR . '/include/autocomplete.php';
	
	
	// V Composer
	if( rd_check_vc_status() ) {
		include( LIBERO_PLUGIN_DIR. '/vc_functions.php');
	}
	
	/*	Add custom control for Visual Composer	*/
	
	if ( class_exists('WPBakeryVisualComposerAbstract') ) {
		
		// Add Type
		require LIBERO_PLUGIN_DIR . '/vc_extend/type/term.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/type/terms.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/type/animate.php';
		
		// Add Shortcode
		require LIBERO_PLUGIN_DIR . '/vc_extend/heading.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/quote.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/map.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/alert.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/button.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/paragraph_with_icon.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/category-box.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/promobox.php';
		
		// Add Shortcode Blog
		require LIBERO_PLUGIN_DIR . '/vc_extend/blog-classic.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/blog-medium.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/blog-box.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/blog-large-grid.php';
		
		// Add Shortcode Horizontal
		require LIBERO_PLUGIN_DIR . '/vc_extend/horizontal-1.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/horizontal-2.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/horizontal-3.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/horizontal-4.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/horizontal-5.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/horizontal-6.php';
		
		// Add Shortcode Slider
		require LIBERO_PLUGIN_DIR . '/vc_extend/slider-large.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/slider-large-2.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/slider-large-3.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/slider-carousel.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/slider-carousel-2.php';
		require LIBERO_PLUGIN_DIR . '/vc_extend/slider-carousel-3.php';
		
	}
	
}
add_action('init','libero_js_composer_init', 99);
	 