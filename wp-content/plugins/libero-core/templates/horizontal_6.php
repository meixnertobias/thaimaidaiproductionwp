<?php
	global $libero_attr, $post;
	
	$libero_query = libero_query_posts($libero_attr);
	if( $libero_attr['number'] ) {
		$libero_query['posts_per_page'] = $libero_attr['number'];
	}
	$args_first = $libero_query;
	$args_first['posts_per_page'] = 1;
	
	$the_query = new WP_Query( $args_first );
	if ( $the_query->have_posts() ) :
?>
<div class="row">
				
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<div class="col-sm-6 libero_border_right">
		
		<!-- Category style 2 -->
		<div class="libero_featured_post">
			
			<?php if( has_post_thumbnail() ) { ?>
			<div class="libero_featured_thumbnail">
				<?php the_post_thumbnail( 'libero-medium' ) ?>
				<div class="libero_thumbnail_overlay">
					<a class="libero_readmore" href="<?php the_permalink() ?>"><?php esc_html_e('Read more','libero') ?></a>
				</div>
			</div>
			<?php } ?>
			
			<div class="libero_featured_content">
				
				<div class="libero_content_meta">
					<span class="libero_category_name"><?php the_category(', '); ?></span>
				</div>
				
				<h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
				
				<div class="libero-post-info">
					<?php libero_post_info() ?>
				</div>
				
				<?php echo libero_get_excerpt_by_id($post->ID,$libero_attr['excerpt_length']); ?>
				
				<div class="libero_category_share">
					<a class="libero_continue_reading" href="<?php the_permalink() ?>"><?php esc_html_e('Continue Reading','libero') ?></a>
					<div class="libero_share_icons text-right">
						<?php get_template_part( 'template-parts/share', 'post' ); ?>
					</div>
					<div class="clear"></div>
				</div>
				
			</div>
		</div>
		<!-- .Category style 2 -->
	</div>	
	<?php endwhile; ?>
	
	<?php 
		if( $libero_attr['number'] > 1 ) :
		$libero_query['posts_per_page'] = $libero_attr['number'] - 1;
		$libero_query['offset'] = 1;
		$the_query = new WP_Query( $libero_query );
		if ( $the_query->have_posts() ) :
		$key = -1;
	?>
	<div class="col-sm-6">
			
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $key++; ?>
			<?php 
				if( $key != 0 && $key % 2 == 0 ){ 
				 echo '</div>';
				}
				if( $key % 2 == 0 ){
				 echo '<div class="row libero-row">';
				}
			?>
			<div class="col-sm-6">
				<div class="libero_featured_post libero_small_post">
					
					<?php if( has_post_thumbnail() ) { ?>
					<div class="libero_featured_thumbnail">
						<?php the_post_thumbnail( 'libero-medium' ) ?>
						<div class="libero_thumbnail_overlay">
							<a class="libero_readmore" href="<?php the_permalink() ?>"><?php esc_html_e('Read more','libero') ?></a>
						</div>
					</div>
					<?php } ?>
					
					<div class="libero_featured_content">
						<h5><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
						<div class="libero-post-info">
							<?php libero_post_info() ?>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; ?>
			<?php
				if( $key % 2 != 0 ){
				 echo '</div>';
				}
			?>
	</div><!-- Horizontal 2 - Right -->
	<?php endif; endif; ?>
	
</div>
<?php endif; ?>