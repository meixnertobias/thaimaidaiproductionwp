<?php
	global $libero_attr, $post;
	$libero_query = libero_query_posts($libero_attr);
	if( $libero_attr['number'] ) {
		$libero_query['posts_per_page'] = $libero_attr['number'];
	}
?>
<div class="owl-carousel libero_category_style_4">
	<?php
		$args_first = $libero_query;
		$args_first['posts_per_page'] = ( $libero_attr['number'] - $libero_attr['columns'] > 0 ? $libero_attr['number'] - $libero_attr['columns'] : $libero_attr['number'] );
		
		$the_query = new WP_Query( $args_first );
		if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) : $the_query->the_post();
	?>
	<div class="libero_owl_item">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'libero-medium' ) ?></a>
		<div class="libero_owl_item_info">
			<div class="libero_owl_item_info_meta text-center">
				<span class="libero_category_name"><?php the_category(', '); ?></span>
				<h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
				<?php libero_post_info(true,false) ?>
			</div>
		</div>
	</div>
	<?php 
		endwhile;
		endif; ?>
</div>
<?php 
	if( $libero_attr['number'] > $libero_attr['columns'] ) :
		$libero_query['posts_per_page'] = $libero_attr['columns'];
		$libero_query['offset'] = $libero_attr['columns'];
		$the_query = new WP_Query( $libero_query );
		if ( $the_query->have_posts() ) :
?>
<div class="libero_category_style_4_list_post libero_container">
	<div class="libero_row">
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="col-sm-<?php echo 12/$libero_attr['columns'] ?> libero_col">
			<div class="libero_featured_post libero_small_post">
				<div class="libero_featured_thumbnail">
					<?php the_post_thumbnail( 'libero-medium' ) ?>
					<div class="libero_thumbnail_overlay">
						<a class="libero_readmore" href="<?php the_permalink() ?>"><?php esc_html_e('Read more','libero') ?></a>
					</div>
				</div>
				<div class="libero_featured_content">
					<h5><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
					<div class="libero-post-info">
						<?php libero_post_info() ?>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
		<div class="clear"></div>
	</div>
</div>
<?php endif; endif; ?>