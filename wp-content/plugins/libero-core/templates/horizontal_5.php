<?php
	global $libero_attr, $post;
	
	$libero_query = libero_query_posts($libero_attr);
	if( $libero_attr['number'] ) {
		$libero_query['posts_per_page'] = $libero_attr['number'];
	}
	$args_first = $libero_query;
	$args_first['posts_per_page'] = 1;
	
	$the_query = new WP_Query( $args_first );
	if ( $the_query->have_posts() ) :
	while ( $the_query->have_posts() ) : $the_query->the_post();
?>

<!-- First Post -->
<div class="libero_featured_post">
	<?php if( has_post_thumbnail() ) { ?>
	<div class="libero_featured_thumbnail">
		<?php the_post_thumbnail( 'libero-medium' ) ?>
		<div class="libero_thumbnail_overlay">
			<a class="libero_readmore" href="<?php the_permalink() ?>"><?php esc_html_e('Read more','libero') ?></a>
		</div>
	</div>
	<?php } ?>
	<div class="libero_featured_content">
		<div class="libero_content_meta">
			<span class="libero_category_name"><?php the_category(', '); ?></span>
		</div>
		<h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
		<div class="libero-post-info">
			<?php libero_post_info() ?>
		</div>
		<?php echo libero_get_excerpt_by_id($post->ID,$libero_attr['excerpt_length']); ?>
		<div class="libero_category_share">
			<a class="libero_continue_reading" href="<?php the_permalink() ?>"><?php esc_html_e('Continue Reading','libero') ?></a>
			<div class="libero_share_icons text-right">
				<?php get_template_part( 'template-parts/share', 'post' ); ?>
			</div>
			<div class="clear"></div>
		</div>
		
	</div>
</div>
<!-- .First Post -->

<?php 
	endwhile;
	endif; ?>

<?php 
	if( $libero_attr['number'] > 1 ) :
	$libero_query['posts_per_page'] = $libero_attr['number'] - 1;
	$libero_query['offset'] = 1;
	$the_query = new WP_Query( $libero_query );
	if ( $the_query->have_posts() ) :
?>
	<div class="libero_category_list_post">
		<ul>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<li>
				<div class="libero-post-info">
					<h6 class="libero-post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h6>
					<?php libero_post_info() ?>
				</div>
			</li>
			<?php endwhile; ?>
		</ul>
	</div>
<!-- .List post -->
<?php endif; endif; ?>