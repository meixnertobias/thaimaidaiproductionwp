<?php
if( ! function_exists( 'libero_vc_quote' ) ) {
	function libero_vc_quote( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'quote'							=> '',
			'style'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['quote'] = ( isset($attr['quote']) && !empty($attr['quote']) ) ? $attr['quote'] : 'This is a premiom and responsive theme, absolutely suited for all your web-based needs.';
		$attr['style'] = ( isset($attr['style']) && !empty($attr['style']) ) ? $attr['style'] : '';
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		if( $attr['style'] ) {
			$class[] = $attr['style'];
		}
		if( $attr['extraclassname'] ) {
			$class[] = $attr['extraclassname'];
		}
		ob_start();
	?>
		<blockquote class="<?php echo libero_class($class) ?>"><?php echo $attr['quote'] ?></blockquote>
	<?php
		return ob_get_clean();
	}
}
add_shortcode( 'libero_quote', 'libero_vc_quote' );

vc_map( array (
	'base' 			=> 'libero_quote',
	'name' 			=> __('Block Quote', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'quote',
			'type' 			=> 'textarea',
			'heading' 		=> __('Quote Content', 'mfn-opts'),
			'value'			=> 'This is a premiom and responsive theme, absolutely suited for all your web-based needs.',
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'style',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Style', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
				"Style 1" => "",
				"Style 2" => "libero-quote-style-2",
				"Style 3" => "libero-quote-style-3"
			),				
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
