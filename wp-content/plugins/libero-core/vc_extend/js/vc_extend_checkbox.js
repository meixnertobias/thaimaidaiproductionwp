jQuery(document).ready(function($){
	$('input[type="checkbox"]').click(function(){
		if( $(this).attr('rst-data-input') != '' ){
			var data_input = $(this).attr('rst-data-input');
			if( $(this).is(':checked') )
				$('input[rst-data-name="'+ data_input +'"]').val( add_id($('input[rst-data-name="'+ data_input +'"]').val(),$(this).val()) );
			else 
				$('input[rst-data-name="'+ data_input +'"]').val( remove_id($('input[rst-data-name="'+ data_input +'"]').val(),$(this).val()) );
		}
	});
});
function add_id(value,id){
	if(value == '')
		value = id;
	else
		value = value + ',' + id;
	return value;
}

function remove_id(value,id){
	if(value == null)
		return;
	else {
		array_value = value.split(",");
		value = '';
		array_value.forEach(function(entry) {
			if( entry != id )
				value += entry+',';
		})
	}
	if(value.substr(0,1)==',')
		value = value.substr(1);
	if(value.substr(value.length-1,value.length)==',')
		value = value.substr(0,value.length-1);
	return value;
}