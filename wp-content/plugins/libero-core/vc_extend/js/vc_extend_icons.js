jQuery(document).ready(function($){
	$('.rst-icons-wrap input.rst_icons_field').focus(function(){
		$(this).next().slideDown(300);
	});
	$('.rst-icons-wrap input.rst_icons_field').focusout(function(){
		$(this).next().slideUp(300);
	});
	$('.rst-list-icons a').click(function(e){
		e.preventDefault();
		$(this).parents('.rst-icons-wrap').find('.rst-list-icons').slideUp(300);
		$(this).parents('.rst-icons-wrap').find('input.rst_icons_field').val( $(this).find('i').attr('class') );
	});
});