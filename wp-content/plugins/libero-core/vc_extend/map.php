<?php
if( ! function_exists( 'lz_vc_map' ) ) {
	function lz_vc_map( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'address'								=> '',
			'mapzoom'								=> '',
			'extraclassname'						=> '',
			
		), $attr));
		
		
		$attr['address'] = ( isset($attr['address']) && !empty($attr['address']) ) ? $attr['address'] : '40.705326,-74.010272';
		$attr['height'] = ( isset($attr['height']) && !empty($attr['height']) ) ? absint($attr['height']) : '420';
		$attr['mapzoom'] = ( isset($attr['mapzoom']) && !empty($attr['mapzoom']) ) ? $attr['mapzoom'] : '15';
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		$class[] = 'rst-contact';
		$class[] = $attr['extraclassname'];
		ob_start();
		if( $attr['address'] ) {
	?>
		<div id="cd-google-map" data-center="<?php echo $attr['address'] ?>" data-zoom="<?php echo $attr['mapzoom'] ?>" style="<?php echo $attr['height'] ? "height:". $attr['height'] . "px" : "" ?>">
			<div id="google-container"></div>
		</div>
	<?php
		}
		return ob_get_clean();
	}
}
add_shortcode( 'lz_map', 'lz_vc_map' );

vc_map( array (
	'base' 			=> 'lz_map',
	'name' 			=> __('Map', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'address',
			'heading' 		=> __('Address - latitude & longitude', 'mfn-opts'),
			'description'   => 'Go to http://www.latlong.net and put the name of a place, city, state, or address, or click the location on the map to get lat long coordinates <br/>eg: 40.705326,-74.010272',                                                     
			'type' 			=> 'textfield',
			'admin_label'	=> true,
			'value'			=> '40.705326,-74.010272'
		),
		array (
			'param_name' 	=> 'mapzoom',
			'type' 			=> 'textfield',
			'heading' 		=> __('Map Zoom', 'mfn-opts'),
			'value'			=> '15',
			'admin_label'	=> true,
			'description'   => '',
		),
		array (
			'param_name' 	=> 'height',
			'type' 			=> 'textfield',
			'heading' 		=> __('Height', 'mfn-opts'),
			'value'			=> '420',
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
