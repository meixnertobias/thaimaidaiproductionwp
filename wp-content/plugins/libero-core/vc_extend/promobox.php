<?php
if( ! function_exists( 'lz_vc_promobox' ) ) {
	function lz_vc_promobox( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'style'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = ( isset($attr['title']) && !empty($attr['title']) ) ? $attr['title'] : 'Title Promobox.';
		$attr['style'] = ( isset($attr['style']) && !empty($attr['style']) ) ? $attr['style'] : 'libero_promobox_style_1';
		$attr['button_text'] = ( isset($attr['button_text']) && !empty($attr['button_text']) ) ? $attr['button_text'] : '';
		$attr['button_link'] = ( isset($attr['button_link']) && !empty($attr['button_link']) ) ? $attr['button_link'] : '';
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		$class[] = 'libero_promobox';
		if( $attr['style'] ) {
			$class[] = $attr['style'];
		}
		$class[] = $attr['extraclassname'];
		
		$html .= '<div class="'. libero_class($class) .'">';
			if( $attr['title'] ) {
				$html .= '<h3>'. $attr['title'] .'</h3>';
			}
			$html .= wpautop($content);
			if( $attr['button_text'] ) {
				$html .= '<a href="'. esc_url($attr['button_link']) .'">'. $attr['button_text'] .'</a>';
			}
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_promobox', 'lz_vc_promobox' );

vc_map( array (
	'base' 			=> 'lz_promobox',
	'name' 			=> __('Promobox', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 'Title Promobox.'
		),
		array (
			'param_name' 	=> 'content',
			'type' 			=> 'textarea_html',
			'heading' 		=> __('Content', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 'Alert message. You message got here.'
		),
		array (
			'param_name' 	=> 'style',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Style', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
							__("Style 1", LANGUAGE_ZONE) => "libero_promobox_style_1",
							__("Style 2", LANGUAGE_ZONE) => "libero_promobox_style_2",
							__("Style 3", LANGUAGE_ZONE) => "libero_promobox_style_3",
							__("Style 4", LANGUAGE_ZONE) => "libero_promobox_style_4",
							__("Style 5", LANGUAGE_ZONE) => "libero_promobox_style_5",
							__("Style 6", LANGUAGE_ZONE) => "libero_promobox_style_6",
			),
		),
		array (
			'param_name' 	=> 'button_text',
			'type' 			=> 'textfield',
			'heading' 		=> __('Button Text', 'mfn-opts'),
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'button_link',
			'type' 			=> 'textfield',
			'heading' 		=> __('Button Link', 'mfn-opts'),
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
