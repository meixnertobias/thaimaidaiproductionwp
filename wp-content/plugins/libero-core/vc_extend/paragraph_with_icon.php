<?php
if( ! function_exists( 'libero_vc_paragraph_with_icon' ) ) {
	function libero_vc_paragraph_with_icon( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'type'							=> '',
			'heading'						=> '',
			'style'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$content = !empty($content) ? $content : '&nbsp;';
		$attr['type'] = ( isset($attr['type']) && !empty($attr['type']) ) ? $attr['type'] : 'fontawesome';
		$attr['icon_fontawesome'] = ( isset($attr['icon_fontawesome']) && !empty($attr['icon_fontawesome']) ) ? $attr['icon_fontawesome'] : '';
		$attr['icon_openiconic'] = ( isset($attr['icon_openiconic']) && !empty($attr['icon_openiconic']) ) ? $attr['icon_openiconic'] : '';
		$attr['icon_typicons'] = ( isset($attr['icon_typicons']) && !empty($attr['icon_typicons']) ) ? $attr['icon_typicons'] : '';
		$attr['icon_entypo'] = ( isset($attr['icon_entypo']) && !empty($attr['icon_entypo']) ) ? $attr['icon_entypo'] : '';
		$attr['icon_linecons'] = ( isset($attr['icon_linecons']) && !empty($attr['icon_linecons']) ) ? $attr['icon_linecons'] : '';
		$attr['heading'] = ( isset($attr['heading']) && !empty($attr['heading']) ) ? $attr['heading'] : 'Heading';
		$attr['style'] = ( isset($attr['style']) && !empty($attr['style']) ) ? $attr['style'] : 1;
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		if( $attr['style'] == 1 ) {
			$class[] = 'libero_contact_box';
			$class[] = 'text-center';
		}
		else {
			$class[] = 'libero_contact_detail_list';
		}
		if( $attr['extraclassname'] ) {
			$class[] = $attr['extraclassname'];
		}
		
		$type = "icon_". $attr['type'];
		$iconClass = isset( $attr[$type] ) ? esc_attr($attr[$type] ) : '';
		
		ob_start();
	?>
		<div class="<?php echo libero_class($class) ?>">
			
			<?php if( $attr['style'] == 1 && $iconClass ) { ?>
			<i class="libero_icon vc_icon_element-icon <?php echo esc_attr($iconClass); ?>"></i>
			<?php } ?>
			
			<?php if( $attr['style'] == 1 && $attr['heading'] ) { ?>
			<h5><?php echo $attr['heading'] ?></h5>
			<?php } ?>
			
			
			<div class="libero_detail">
				
				<?php if( $attr['style'] == 2 && $iconClass ) { ?>
				<i class="libero_icon vc_icon_element-icon <?php echo esc_attr($iconClass); ?>"></i>
				<?php } ?>
				
				<?php 
					if( $attr['style'] == 1 ) {
						echo wpautop($content); 
					}
					else {
						echo '<span>'. $content .'</span>'; 
					}
				?>
			</div>
			
		</div>
	<?php
		return ob_get_clean();
	}
}
add_shortcode( 'libero_paragraph_with_icon', 'libero_vc_paragraph_with_icon' );

vc_map( array (
	'base' 			=> 'libero_paragraph_with_icon',
	'name' 			=> __('Paragraph With Icon', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'style',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Style', 'mfn-opts'),
			"value" 		=> array(
				"Has Border, icon center" => "1",
				"No Border, icon left" => "2",
			),
			'admin_label'	=> true,
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Icon library', 'js_composer' ),
			'value' => array(
				__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
				__( 'Open Iconic', 'js_composer' ) => 'openiconic',
				__( 'Typicons', 'js_composer' ) => 'typicons',
				__( 'Entypo', 'js_composer' ) => 'entypo',
				__( 'Linecons', 'js_composer' ) => 'linecons',
			),
			'admin_label' => true,
			'param_name' => 'type',
			'description' => __( 'Select icon library.', 'js_composer' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'js_composer' ),
			'param_name' => 'icon_fontawesome',
			'value' => 'fa fa-adjust', // default value to backend editor admin_label
			'settings' => array(
				'emptyIcon' => false,
				// default true, display an "EMPTY" icon?
				'iconsPerPage' => 4000,
				// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
			),
			'dependency' => array(
				'element' => 'type',
				'value' => 'fontawesome',
			),
			'description' => __( 'Select icon from library.', 'js_composer' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'js_composer' ),
			'param_name' => 'icon_openiconic',
			'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'openiconic',
				'iconsPerPage' => 4000, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'type',
				'value' => 'openiconic',
			),
			'description' => __( 'Select icon from library.', 'js_composer' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'js_composer' ),
			'param_name' => 'icon_typicons',
			'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'typicons',
				'iconsPerPage' => 4000, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'type',
				'value' => 'typicons',
			),
			'description' => __( 'Select icon from library.', 'js_composer' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'js_composer' ),
			'param_name' => 'icon_entypo',
			'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'entypo',
				'iconsPerPage' => 4000, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'type',
				'value' => 'entypo',
			),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'js_composer' ),
			'param_name' => 'icon_linecons',
			'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'linecons',
				'iconsPerPage' => 4000, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'type',
				'value' => 'linecons',
			),
			'description' => __( 'Select icon from library.', 'js_composer' ),
		),
		array (
			'param_name' 	=> 'heading',
			'type' 			=> 'textfield',
			'heading' 		=> __('Heading', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 'Heading',
			'dependency' => array(
				'element' => 'style',
				'value' => '1',
			),
		),
		array (
			'param_name' 	=> 'content',
			'type' 			=> 'textarea_html',
			'heading' 		=> __('Content', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
