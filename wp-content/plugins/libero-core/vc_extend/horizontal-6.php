<?php
if( ! function_exists( 'libero_vc_horizontal_6' ) ) {
	function libero_vc_horizontal_6( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'categories'					=> '',
			'tag_slug'						=> '',
			'authors_id'					=> '',
			'ids'							=> '',
			'sort'							=> '',
			'number'						=> '',
			'hidden_category'				=> '',
			'excerpt_length'				=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = ( isset($attr['title']) && !empty($attr['title']) ) ? $attr['title'] : '';
		
		$attr['categories'] = ( isset($attr['categories']) && !empty($attr['categories']) ) ? $attr['categories'] : '';
		$attr['tag_slug'] = ( isset($attr['tag_slug']) && !empty($attr['tag_slug']) ) ? $attr['tag_slug'] : '';
		$attr['authors_id'] = ( isset($attr['authors_id']) && !empty($attr['authors_id']) ) ? $attr['authors_id'] : '';
		$attr['ids'] = ( isset($attr['ids']) && !empty($attr['ids']) ) ? $attr['ids'] : '';
		$attr['sort'] = ( isset($attr['sort']) && !empty($attr['sort']) ) ? $attr['sort'] : '';
		
		$attr['number'] = ( isset($attr['number']) && !empty($attr['number']) ) ? absint($attr['number']) : 5;
		$attr['hidden_category'] = ( isset($attr['hidden_category']) && !empty($attr['hidden_category']) ) ? $attr['hidden_category'] : false;
		$attr['excerpt_length'] = ( isset($attr['excerpt_length']) && !empty($attr['excerpt_length']) ) ? absint($attr['excerpt_length']) : 30;
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		$class[] = 'libero_category_standar';
		$class[] = 'libero_horizontal_6';
		$class[] = 'libero-wrap-ajax';
		
		if( $attr['extraclassname'] ) {
			$class[] = $attr['extraclassname'];
		}
		ob_start();
		
		global $libero_attr;
		$libero_attr = $attr;
	?>
		<div class="<?php echo libero_class($class) ?>">
			
			<?php $liberokey = uniqid(); ?>
			<?php libero_render_script_ajax_block('horizontal_6',$libero_attr, $liberokey); ?>
			
			<div class="libero_title_with_breadcrumb">
				<?php
					if( ! $attr['hidden_category'] ) {
						libero_render_categories($libero_attr, $liberokey);
					}
				?>
				<?php if($attr['title']) { ?>
				<h4 class="libero_title"><?php echo $attr['title'] ?></h4>
				<?php } ?>
			</div>
			<div class="libero-inner-ajax">
				<?php libero_get_template_part('loading') ?>
				<div id="<?php echo $liberokey ?>-all" class="tab-ajax">
					<?php libero_get_template_part('horizontal_6'); ?>
				</div>
			</div>
		</div>
	<?php
		wp_reset_postdata();
		wp_reset_query();
		return ob_get_clean();
	}
}
add_shortcode( 'libero_horizontal_6', 'libero_vc_horizontal_6' );

vc_map( array (
	'base' 			=> 'libero_horizontal_6',
	'name' 			=> __('Horizontal 6', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> ''
		),
		array (
			'param_name' 	=> 'categories',
			'type' 			=> 'terms',
			'heading' 		=> __('Category filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Multiple category filter.',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'tag_slug',
			'type' 			=> 'textfield',
			'heading' 		=> __('Filter by tag slug:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'To filter multiple tag slugs, enter here the tag slugs separated by commas (ex: tag1,tag2,tag3).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'authors_id',
			'type' 			=> 'textfield',
			'heading' 		=> __('Multiple authors filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Filter multiple authors by ID. Enter here the author IDs separated by commas (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'ids',
			'type' 			=> 'autocomplete',
			'heading' 		=> __('Include Posts:', 'mfn-opts'),
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
				'unique_values' => true,
			),
			'admin_label'	=> true,
			'description'	=> 'Filter by post ID (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'sort',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Sort order:', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
				"- Latest -" 	=> "",
				"Popular (all time)" 		=> "popular",
				"Popular (last 7 days)" 	=> "popular7",
				"Random Posts" 				=> "random_posts",
				"Random posts Today" 		=> "random_today",
				"Random posts from last 7 Day" 	=> "random_7_day",
				"Most Commented" 			=> "comment_count"
			),
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'number',
			'type' 			=> 'textfield',
			'heading' 		=> __('Limit post number', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 5
		),
		array (
			'param_name' 	=> 'hidden_category',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Category Top Title', 'mfn-opts'),
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'excerpt_length',
			'type' 			=> 'textfield',
			'heading' 		=> __('Excerpt Length', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 30
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));

//Filters For autocomplete param:
//For suggestion: vc_autocomplete_[shortcode_name]_[param_name]_callback
add_filter( 'vc_autocomplete_libero_horizontal_6_ids_callback', 'postIdsAutocompleteSuggester', 10, 1 ); // Get suggestion(find). Must return an array
add_filter( 'vc_autocomplete_libero_horizontal_6_ids_render', 'postIdsAutocompleteRender', 10, 1 ); // Render exact product. Must return an array (label,value)