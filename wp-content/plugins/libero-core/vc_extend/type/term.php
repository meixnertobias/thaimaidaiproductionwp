<?php 

vc_add_shortcode_param( 'term', 'rst_term_settings_field', LIBERO_PLUGIN_URL . '/vc_extend/js/vc_extend_checkbox.js' );
function rst_term_settings_field( $settings, $value ) {
	$html = '';
	$taxonomies = isset($settings['taxonomies']) ? $settings['taxonomies'] : 'category'; // name taxonomy
	$hide_empty = isset($settings['hide_empty']) ? $settings['hide_empty'] : '0'; // 0 or 1
	$terms = get_terms( $taxonomies, array(
		'orderby'    	=> 'term_group',
		'order' 		=> 'ASC',
		'hide_empty' 	=> $hide_empty
	));
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
		
		$html .= '<select name="'. esc_attr( $settings['param_name'] ) .'" class="wpb_vc_param_value wpb-textinput ' . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" value="' . esc_attr( $value ) . '">';
			
			foreach ( $terms as $key=>$term ) {
				$html .= '<option '. selected($term->slug,$value,false) .' value="'. $term->slug .'">'. $term->name .'</option>';
			}
			
		$html .= '</select>';
	}
	else {
		$html = 'Sorry, but nothing matched your search terms.';
	}
	return $html;
}