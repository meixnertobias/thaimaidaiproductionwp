<?php
vc_add_shortcode_param( 'animate', 'rst_animates_settings_field' );
function rst_animates_settings_field( $settings, $value ) {
	$html = '';
	
	$html .= '<select class="wpb_vc_param_value" name="'. esc_attr( $settings['param_name'] ) .'">';
		$html .= '<option '. selected('bounce',$value) .' value="">Select Animate</option>';
		
        $html .= '<optgroup label="Attention Seekers">';
          $html .= '<option '. selected('bounce',$value) .' value="bounce">bounce</option>';
          $html .= '<option '. selected('flash',$value) .' value="flash">flash</option>';
          $html .= '<option '. selected('pulse',$value) .' value="pulse">pulse</option>';
          $html .= '<option '. selected('rubberBand',$value) .' value="rubberBand">rubberBand</option>';
          $html .= '<option '. selected('shake',$value) .' value="shake">shake</option>';
          $html .= '<option '. selected('swing',$value) .' value="swing">swing</option>';
          $html .= '<option '. selected('tada',$value) .' value="tada">tada</option>';
          $html .= '<option '. selected('wobble',$value) .' value="wobble">wobble</option>';
          $html .= '<option '. selected('jello',$value) .' value="jello">jello</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Bouncing Entrances">';
          $html .= '<option '. selected('bounceIn',$value) .' value="bounceIn">bounceIn</option>';
          $html .= '<option '. selected('bounceInDown',$value) .' value="bounceInDown">bounceInDown</option>';
          $html .= '<option '. selected('bounceInLeft',$value) .' value="bounceInLeft">bounceInLeft</option>';
          $html .= '<option '. selected('bounceInRight',$value) .' value="bounceInRight">bounceInRight</option>';
          $html .= '<option '. selected('bounceInUp',$value) .' value="bounceInUp">bounceInUp</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Bouncing Exits">';
          $html .= '<option '. selected('bounceOut',$value) .' value="bounceOut">bounceOut</option>';
          $html .= '<option '. selected('bounceOutDown',$value) .' value="bounceOutDown">bounceOutDown</option>';
          $html .= '<option '. selected('bounceOutLeft',$value) .' value="bounceOutLeft">bounceOutLeft</option>';
          $html .= '<option '. selected('bounceOutRight',$value) .' value="bounceOutRight">bounceOutRight</option>';
          $html .= '<option '. selected('bounceOutUp',$value) .' value="bounceOutUp">bounceOutUp</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Fading Entrances">';
          $html .= '<option '. selected('fadeIn',$value) .' value="fadeIn">fadeIn</option>';
          $html .= '<option '. selected('fadeInDown',$value) .' value="fadeInDown">fadeInDown</option>';
          $html .= '<option '. selected('fadeInDownBig',$value) .' value="fadeInDownBig">fadeInDownBig</option>';
          $html .= '<option '. selected('fadeInLeft',$value) .' value="fadeInLeft">fadeInLeft</option>';
          $html .= '<option '. selected('fadeInLeftBig',$value) .' value="fadeInLeftBig">fadeInLeftBig</option>';
          $html .= '<option '. selected('fadeInRight',$value) .' value="fadeInRight">fadeInRight</option>';
          $html .= '<option '. selected('fadeInRightBig',$value) .' value="fadeInRightBig">fadeInRightBig</option>';
          $html .= '<option '. selected('fadeInUp',$value) .' value="fadeInUp">fadeInUp</option>';
          $html .= '<option '. selected('fadeInUpBig',$value) .' value="fadeInUpBig">fadeInUpBig</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Fading Exits">';
          $html .= '<option '. selected('fadeOut',$value) .' value="fadeOut">fadeOut</option>';
          $html .= '<option '. selected('fadeOutDown',$value) .' value="fadeOutDown">fadeOutDown</option>';
          $html .= '<option '. selected('fadeOutDownBig',$value) .' value="fadeOutDownBig">fadeOutDownBig</option>';
          $html .= '<option '. selected('fadeOutLeft',$value) .' value="fadeOutLeft">fadeOutLeft</option>';
          $html .= '<option '. selected('fadeOutLeftBig',$value) .' value="fadeOutLeftBig">fadeOutLeftBig</option>';
          $html .= '<option '. selected('fadeOutRight',$value) .' value="fadeOutRight">fadeOutRight</option>';
          $html .= '<option '. selected('fadeOutRightBig',$value) .' value="fadeOutRightBig">fadeOutRightBig</option>';
          $html .= '<option '. selected('fadeOutUp',$value) .' value="fadeOutUp">fadeOutUp</option>';
          $html .= '<option '. selected('fadeOutUpBig',$value) .' value="fadeOutUpBig">fadeOutUpBig</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Flippers">';
          $html .= '<option '. selected('flip',$value) .' value="flip">flip</option>';
          $html .= '<option '. selected('flipInX',$value) .' value="flipInX">flipInX</option>';
          $html .= '<option '. selected('flipInY',$value) .' value="flipInY">flipInY</option>';
          $html .= '<option '. selected('flipOutX',$value) .' value="flipOutX">flipOutX</option>';
          $html .= '<option '. selected('flipOutY',$value) .' value="flipOutY">flipOutY</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Lightspeed">';
          $html .= '<option '. selected('lightSpeedIn',$value) .' value="lightSpeedIn">lightSpeedIn</option>';
          $html .= '<option '. selected('lightSpeedOut',$value) .' value="lightSpeedOut">lightSpeedOut</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Rotating Entrances">';
          $html .= '<option '. selected('rotateIn',$value) .' value="rotateIn">rotateIn</option>';
          $html .= '<option '. selected('rotateInDownLeft',$value) .' value="rotateInDownLeft">rotateInDownLeft</option>';
          $html .= '<option '. selected('rotateInDownRight',$value) .' value="rotateInDownRight">rotateInDownRight</option>';
          $html .= '<option '. selected('rotateInUpLeft',$value) .' value="rotateInUpLeft">rotateInUpLeft</option>';
          $html .= '<option '. selected('rotateInUpRight',$value) .' value="rotateInUpRight">rotateInUpRight</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Rotating Exits">';
          $html .= '<option '. selected('rotateOut',$value) .' value="rotateOut">rotateOut</option>';
          $html .= '<option '. selected('rotateOutDownLeft',$value) .' value="rotateOutDownLeft">rotateOutDownLeft</option>';
          $html .= '<option '. selected('rotateOutDownRight',$value) .' value="rotateOutDownRight">rotateOutDownRight</option>';
          $html .= '<option '. selected('rotateOutUpLeft',$value) .' value="rotateOutUpLeft">rotateOutUpLeft</option>';
          $html .= '<option '. selected('rotateOutUpRight',$value) .' value="rotateOutUpRight">rotateOutUpRight</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Sliding Entrances">';
          $html .= '<option '. selected('slideInUp',$value) .' value="slideInUp">slideInUp</option>';
          $html .= '<option '. selected('slideInDown',$value) .' value="slideInDown">slideInDown</option>';
          $html .= '<option '. selected('slideInLeft',$value) .' value="slideInLeft">slideInLeft</option>';
          $html .= '<option '. selected('slideInRight',$value) .' value="slideInRight">slideInRight</option>';

        $html .= '</optgroup>';
        $html .= '<optgroup label="Sliding Exits">';
          $html .= '<option '. selected('slideOutUp',$value) .' value="slideOutUp">slideOutUp</option>';
          $html .= '<option '. selected('slideOutDown',$value) .' value="slideOutDown">slideOutDown</option>';
          $html .= '<option '. selected('slideOutLeft',$value) .' value="slideOutLeft">slideOutLeft</option>';
          $html .= '<option '. selected('slideOutRight',$value) .' value="slideOutRight">slideOutRight</option>';
          
        $html .= '</optgroup>';
        
        $html .= '<optgroup label="Zoom Entrances">';
          $html .= '<option '. selected('zoomIn',$value) .' value="zoomIn">zoomIn</option>';
          $html .= '<option '. selected('zoomInDown',$value) .' value="zoomInDown">zoomInDown</option>';
          $html .= '<option '. selected('zoomInLeft',$value) .' value="zoomInLeft">zoomInLeft</option>';
          $html .= '<option '. selected('zoomInRight',$value) .' value="zoomInRight">zoomInRight</option>';
          $html .= '<option '. selected('zoomInUp',$value) .' value="zoomInUp">zoomInUp</option>';
        $html .= '</optgroup>';
        
        $html .= '<optgroup label="Zoom Exits">';
          $html .= '<option '. selected('zoomOut',$value) .' value="zoomOut">zoomOut</option>';
          $html .= '<option '. selected('zoomOutDown',$value) .' value="zoomOutDown">zoomOutDown</option>';
          $html .= '<option '. selected('zoomOutLeft',$value) .' value="zoomOutLeft">zoomOutLeft</option>';
          $html .= '<option '. selected('zoomOutRight',$value) .' value="zoomOutRight">zoomOutRight</option>';
          $html .= '<option '. selected('zoomOutUp',$value) .' value="zoomOutUp">zoomOutUp</option>';
        $html .= '</optgroup>';

        $html .= '<optgroup label="Specials">';
          $html .= '<option '. selected('hinge',$value) .' value="hinge">hinge</option>';
          $html .= '<option '. selected('rollIn',$value) .' value="rollIn">rollIn</option>';
          $html .= '<option '. selected('rollOut',$value) .' value="rollOut">rollOut</option>';
        $html .= '</optgroup>';
      
			
	$html .= '</select>';
	
	return $html;
}