<?php
if( ! function_exists( 'lz_vc_button' ) ) {
	function lz_vc_button( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'link'							=> '',
			'target'						=> '',
			'size'							=> '',
			'style'							=> '',
			'extraclassname'				=> '',
		), $attr));
		
		$attr['title'] = ( isset($attr['title']) && !empty($attr['title']) ) ? $attr['title'] : '';
		$attr['link'] = ( isset($attr['link']) && !empty($attr['link']) ) ? $attr['link'] : '';
		$attr['target'] = ( isset($attr['target']) && !empty($attr['target']) ) ? $attr['target'] : '0';
		$attr['size'] = ( isset($attr['size']) && !empty($attr['size']) ) ? $attr['size'] : 'btn_size_1';
		$attr['style'] = ( isset($attr['style']) && !empty($attr['style']) ) ? $attr['style'] : 'btn_style_1';
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		$class[] = 'btn';
		$class[] = $attr['size'];
		$class[] = $attr['style'];
		
		$blank = $attr['target']=='blank' ? 'target="_blank"' : '';
		$link = $attr['link'] ? 'href="'. esc_url($attr['link']) .'"' : '';
		$html = '';
		
		$html .= '<a '. $blank .' '. $link .' type="button" class="'. libero_class($class) .'">'. $attr['title'] .'</a>';
		
		return $html;
	}
}
add_shortcode( 'lz_button', 'lz_vc_button' );

vc_map( array (
	'base' 			=> 'lz_button',
	'name' 			=> __('Buttons', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Content', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array (
			'param_name' 	=> 'link',
			'type' 			=> 'textfield',
			'heading' 		=> __('Link', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array (
			'param_name' 	=> 'target',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Target', 'mfn-opts'),
			'admin_label'	=> true,        
			"value" 		=> array(
							"Stay in Window" => "0",
							"New Tab" => "blank",
			),				
		),
		array (
			'param_name' 	=> 'size',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Size', 'mfn-opts'),
			'admin_label'	=> true,        
			"value" 		=> array(
							"Very Large" => "btn_size_1",
							"Large" => "btn_size_2",
							"Medium" => "btn_size_3",
							"Small" => "btn_size_4",
							"Normal" => "btn_size_5",
			),
		),
		array (
			'param_name' 	=> 'style',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Style', 'mfn-opts'),
			'admin_label'	=> true,        
			"value" 		=> array(
							"Border Grey" => "btn_style_1",
							"Border Blue" => "btn_style_2",
							"Border Green" => "btn_style_3",
							"Border Brown" => "btn_style_7",
							"Border Yellow" => "btn_style_8",
							"Border Red" => "btn_style_9",
							"Background Grey" => "btn_style_4",
							"Background Blue" => "btn_style_5",
							"Background Green" => "btn_style_6",
							"Background Brown" => "btn_style_10",
							"Background Yellow" => "btn_style_11",
							"Background Red" => "btn_style_12",
			),
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
