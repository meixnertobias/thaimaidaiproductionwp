<?php
if( ! function_exists( 'lz_vc_title' ) ) {
	function lz_vc_title( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'align'							=> '',
			'title_tag'						=> '',
			'size'							=> '',
			'color'							=> '',
			'animate'						=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = ( isset($attr['title']) && !empty($attr['title']) ) ? $attr['title'] : '';
		$attr['align'] = ( isset($attr['align']) && !empty($attr['align']) ) ? $attr['align'] : 'text-left';
		$attr['title_tag'] = ( isset($attr['title_tag']) && !empty($attr['title_tag']) ) ? $attr['title_tag'] : 'h2';
		$attr['color'] = ( isset($attr['color']) && !empty($attr['color']) ) ? $attr['color'] : '';
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['align'] ) {
			$class[] = $attr['align'];
		}
		$class[] = $attr['extraclassname'];
		if( isset($attr['border']) && $attr['border'] ) {
			$class[] = 'libero_title';
		}
		
		$style = '';
		$tag = $attr['title_tag'];
		if( $attr['color'] ) {
			$style = 'style="color:'. $attr['color'] .'"';
		}
		
		$html .= '<'. $tag.' class="'. libero_class($class) .'" '. $style .'>';
			$html .= $attr['title'];
		$html .= '</'. $tag.'>';
		
		return $html;
	}
}
add_shortcode( 'lz_title', 'lz_vc_title' );

vc_map( array (
	'base' 			=> 'lz_title',
	'name' 			=> __('Heading', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array (
			'param_name' 	=> 'align',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Align', 'mfn-opts'),
			'admin_label'	=> true,        
			"value" 		=> array(
							"Left" => "text-left",
							"Center" => "text-center",
							"Right" => "text-right",
			),
		),
		array (
			'param_name' 	=> 'title_tag',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Heading Tag', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
							__("h2", LANGUAGE_ZONE) => "h2", 
							__("h3", LANGUAGE_ZONE) => "h3",
							__("h4", LANGUAGE_ZONE) => "h4",
							__("h5", LANGUAGE_ZONE) => "h5",
							__("h6", LANGUAGE_ZONE) => "h6",
			),
		),
		array (
			'param_name' 	=> 'color',
			'type' 			=> 'colorpicker',
			'heading' 		=> __('Color', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> ''
		),
		array (
			'param_name' 	=> 'border',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Has Border', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
