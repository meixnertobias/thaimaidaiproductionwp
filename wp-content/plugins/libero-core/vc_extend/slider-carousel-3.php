<?php
if( ! function_exists( 'libero_vc_featured_posts_3' ) ) {
	function libero_vc_featured_posts_3( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'categories'					=> '',
			'tag_slug'						=> '',
			'authors_id'					=> '',
			'ids'							=> '',
			'sort'							=> '',
			'style'							=> '',
			'number_column'					=> '',
			'number_posts'					=> '',
			'excerpt_length'				=> '',
			'hidden_category'				=> '',
			'hidden_title'					=> '',
			'hidden_date'					=> '',
			'hidden_excerpt'				=> '',
			'hidden_share'					=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = ( isset($attr['title']) && !empty($attr['title']) ) ? $attr['title'] : '';
		
		$attr['categories'] = ( isset($attr['categories']) && !empty($attr['categories']) ) ? $attr['categories'] : '';
		$attr['tag_slug'] = ( isset($attr['tag_slug']) && !empty($attr['tag_slug']) ) ? $attr['tag_slug'] : '';
		$attr['authors_id'] = ( isset($attr['authors_id']) && !empty($attr['authors_id']) ) ? $attr['authors_id'] : '';
		$attr['ids'] = ( isset($attr['ids']) && !empty($attr['ids']) ) ? $attr['ids'] : '';
		$attr['sort'] = ( isset($attr['sort']) && !empty($attr['sort']) ) ? $attr['sort'] : '';
		
		$attr['style'] = ( isset($attr['style']) && !empty($attr['style']) ) ? absint($attr['style']) : 1;
		$attr['number_column'] = ( isset($attr['number_column']) && !empty($attr['number_column']) ) ? absint($attr['number_column']) : 3;
		$attr['number_posts'] = ( isset($attr['number_posts']) && !empty($attr['number_posts']) ) ? absint($attr['number_posts']) : 8;
		$attr['excerpt_length'] = ( isset($attr['excerpt_length']) && !empty($attr['excerpt_length']) ) ? absint($attr['excerpt_length']) : 20;
		$attr['hidden_category'] = ( isset($attr['hidden_category']) ) ? $attr['hidden_category'] : false;
		$attr['hidden_title'] = ( isset($attr['hidden_title']) ) ? $attr['hidden_title'] : false;
		$attr['hidden_date'] = ( isset($attr['hidden_date']) ) ? $attr['hidden_date'] : false;
		$attr['hidden_excerpt'] = ( isset($attr['hidden_excerpt']) ) ? $attr['hidden_excerpt'] : false;		$attr['hidden_readmore'] = ( isset($attr['hidden_readmore']) ) ? $attr['hidden_readmore'] : false;
		$attr['hidden_share'] = ( isset($attr['hidden_share']) ) ? $attr['hidden_share'] : false;
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		$class[] = 'libero_header_slidershow';
		if( $attr['style']== 1 ) {
			$class[] = 'libero_header_slidershow_style_3';
		} else {
			$class[] = 'libero_header_slidershow_style_3b';
		}
		
		$owl_class = array();
		$owl_class[] = 'owl-carousel';
		if( $attr['style'] == 1 ) {
			$owl_class[] = 'libero_header_slider_3';
		} else if( $attr['style'] == 2 ) {
			$owl_class[] = 'libero_header_slider_3b';
		} else if( $attr['style'] == 3 ) {
			$owl_class[] = 'libero_header_slider_3c';
		}
		
		ob_start();
		
	?>
	<section class="<?php echo libero_class($class) ?>">
		
		<div class="<?php echo libero_class($owl_class) ?>" data-columns="<?php echo absint($attr['number_column']); ?>">
			<?php 
				global $post;
				$args = libero_query_posts($attr);
				if( $attr['number_posts'] ) {
					$args['posts_per_page'] = $attr['number_posts'];
				}
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
			<div class="libero_owl_item">
				<?php if( has_post_thumbnail() ) { ?>
				<?php the_post_thumbnail( 'libero-medium' ) ?>
				<?php } ?>
				<div class="libero_owl_item_info">
					<div class="libero_owl_item_info_meta text-center">
						
						<div class="libero_post_info">
							<?php if( ! $attr['hidden_category'] ) { ?>
							<span class="libero_category_name"><?php the_category(', '); ?></span>
							<?php } ?>
							
							<?php if( ! $attr['hidden_date'] && $attr['style'] != 1 ) { ?>
							<?php libero_post_info(true,false,false) ?>
							<?php } ?>
						</div>
						
						<?php if( ! $attr['hidden_title'] ) { ?>
						<h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
						<?php } ?>												<?php if( ! $attr['hidden_date'] && $attr['style'] == 1 ) { ?>						<?php libero_post_info(true,false,false) ?>						<?php } ?>
						
						<div class="libero_owl_excerpt">
							<?php if( ! $attr['hidden_excerpt'] ) echo libero_get_excerpt_by_id($post->ID,$attr['excerpt_length']); ?>
						</div>
							
						<div class="libero_owl_footer">
							<?php if( ! $attr['hidden_readmore'] ) { ?>
							<a class="libero_continue_reading" href="<?php the_permalink() ?>"><?php esc_html_e('Continue Reading','libero') ?></a>
							<?php } ?>
							
							<?php if( ! $attr['hidden_share'] ) { ?>
							<div class="libero_share_icons text-right">
								<?php get_template_part( 'template-parts/share', 'post' ); ?>
							</div>
							<?php } ?>
						</div>
						
					</div>
				</div>
			</div>
			<?php 
				endwhile;
				endif;
			?>
		</div>
	</section>
	
	<?php
		wp_reset_postdata();
		wp_reset_query();
		return ob_get_clean();
	}
}
add_shortcode( 'libero_featured_posts_3', 'libero_vc_featured_posts_3' );

vc_map( array (
	'base' 			=> 'libero_featured_posts_3',
	'name' 			=> __('Slider Carousel Posts 3', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'style',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Style:', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
				"Style 1" 		=> "1",
				"Style 2" 		=> "2",
				"Style 3" 		=> "3",
			)
		),
		array (
			'param_name' 	=> 'number_column',
			'type' 			=> 'textfield',
			'heading' 		=> __('Number column', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 3,
			'dependency' => array(
				'element' => 'style',
				'value' => array(
					'2',
					'3'
				)
			)
		),
		
		array (
			'param_name' 	=> 'number_posts',
			'type' 			=> 'textfield',
			'heading' 		=> __('Number Posts', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 8
		),
		array (
			'param_name' 	=> 'excerpt_length',
			'type' 			=> 'textfield',
			'heading' 		=> __('Excerpt Length', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '20'
		),
		array (
			'param_name' 	=> 'hidden_category',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Category', 'mfn-opts'),
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'hidden_title',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Title', 'mfn-opts'),
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'hidden_date',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Date', 'mfn-opts'),
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'hidden_excerpt',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Excerpt', 'mfn-opts'),
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'hidden_readmore',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Readmore', 'mfn-opts'),
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'hidden_share',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Share', 'mfn-opts'),
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
		array (
			'param_name' 	=> 'categories',
			'type' 			=> 'terms',
			'heading' 		=> __('Category filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Multiple category filter.',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'tag_slug',
			'type' 			=> 'textfield',
			'heading' 		=> __('Filter by tag slug:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'To filter multiple tag slugs, enter here the tag slugs separated by commas (ex: tag1,tag2,tag3).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'authors_id',
			'type' 			=> 'textfield',
			'heading' 		=> __('Multiple authors filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Filter multiple authors by ID. Enter here the author IDs separated by commas (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'ids',
			'type' 			=> 'autocomplete',
			'heading' 		=> __('Include Posts:', 'mfn-opts'),
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
				'unique_values' => true,
			),
			'admin_label'	=> true,
			'description'	=> 'Filter by post ID (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'sort',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Sort order:', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
				"- Latest -" 	=> "",
				"Popular (all time)" 		=> "popular",
				"Popular (last 7 days)" 	=> "popular7",
				"Random Posts" 				=> "random_posts",
				"Random posts Today" 		=> "random_today",
				"Random posts from last 7 Day" 	=> "random_7_day",
				"Most Commented" 			=> "comment_count"
			),
			'group'			=> 'Filters'
		),
	)
));

//Filters For autocomplete param:
//For suggestion: vc_autocomplete_[shortcode_name]_[param_name]_callback
add_filter( 'vc_autocomplete_libero_featured_posts_3_ids_callback', 'postIdsAutocompleteSuggester', 10, 1 ); // Get suggestion(find). Must return an array
add_filter( 'vc_autocomplete_libero_featured_posts_3_ids_render', 'postIdsAutocompleteRender', 10, 1 ); // Render exact product. Must return an array (label,value)