<?php
if( ! function_exists( 'lz_vc_alert' ) ) {
	function lz_vc_alert( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'type'							=> '',
			'style'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['type'] = ( isset($attr['type']) && !empty($attr['type']) ) ? $attr['type'] : 'alert-general';
		$attr['style'] = ( isset($attr['style']) && !empty($attr['style']) ) ? $attr['style'] : 'libero_alert_style_1';
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		$class[] = 'alert';
		if( $attr['type'] ) {
			$class[] = $attr['type'];
		}
		if( $attr['style'] ) {
			$class[] = $attr['style'];
		}
		$class[] = $attr['extraclassname'];
		
		$html .= '<div class="'. libero_class($class) .'" role="alert">';
			$html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-close"></i></button>';
			switch( $attr['type'] ) {
				case 'alert-general':
					$html .= '<i class="fa fa-check"></i>';
				break;
				case 'alert-info':
					$html .= '<i class="fa fa-info"></i>';
				break;
				case 'alert-success':
					$html .= '<i class="fa fa-check"></i>';
				break;
				case 'alert-warning':
					$html .= '<i class="fa fa-exclamation-triangle"></i>';
				break;
				case 'alert-danger':
					$html .= '<i class="fa fa-exclamation-circle"></i>';
				break;
			}
			$html .= $content;
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_alert', 'lz_vc_alert' );

vc_map( array (
	'base' 			=> 'lz_alert',
	'name' 			=> __('Alert', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'content',
			'type' 			=> 'textarea_html',
			'heading' 		=> __('Content', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 'Alert message. You message got here.'
		),
		array (
			'param_name' 	=> 'type',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Type', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
							__("General", LANGUAGE_ZONE) => "alert-general", 
							__("Info", LANGUAGE_ZONE) => "alert-info",
							__("Success", LANGUAGE_ZONE) => "alert-success",
							__("Warning", LANGUAGE_ZONE) => "alert-warning",
							__("Error", LANGUAGE_ZONE) => "alert-danger",
			),
		),
		array (
			'param_name' 	=> 'style',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Style', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
							__("Style 1", LANGUAGE_ZONE) => "libero_alert_style_1",
							__("Style 2", LANGUAGE_ZONE) => "libero_alert_style_2",
							__("Style 3", LANGUAGE_ZONE) => "libero_alert_style_3",
							__("Style 4", LANGUAGE_ZONE) => "libero_alert_style_4",
							__("Style 5", LANGUAGE_ZONE) => "libero_alert_style_5",
			),
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
