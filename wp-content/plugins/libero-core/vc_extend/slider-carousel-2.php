<?php
if( ! function_exists( 'libero_vc_featured_posts_2' ) ) {
	function libero_vc_featured_posts_2( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'categories'					=> '',
			'tag_slug'						=> '',
			'authors_id'					=> '',
			'ids'							=> '',
			'sort'							=> '',
			'number_posts'					=> '',
			'columns'						=> '',
			'excerpt_length'				=> '',
			'hidden_featured_image'			=> '',
			'hidden_category'				=> '',
			'hidden_title'					=> '',
			'hidden_date'					=> '',
			'hidden_excerpt'				=> '',
			'hidden_readmore'				=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = ( isset($attr['title']) && !empty($attr['title']) ) ? $attr['title'] : '';
		
		$attr['categories'] = ( isset($attr['categories']) && !empty($attr['categories']) ) ? $attr['categories'] : '';
		$attr['tag_slug'] = ( isset($attr['tag_slug']) && !empty($attr['tag_slug']) ) ? $attr['tag_slug'] : '';
		$attr['authors_id'] = ( isset($attr['authors_id']) && !empty($attr['authors_id']) ) ? $attr['authors_id'] : '';
		$attr['ids'] = ( isset($attr['ids']) && !empty($attr['ids']) ) ? $attr['ids'] : '';
		$attr['sort'] = ( isset($attr['sort']) && !empty($attr['sort']) ) ? $attr['sort'] : '';
		
		$attr['number_posts'] = ( isset($attr['number_posts']) && !empty($attr['number_posts']) ) ? absint($attr['number_posts']) : 8;
		$attr['columns'] = ( isset($attr['columns']) && !empty($attr['columns']) ) ? $attr['columns'] : 4;
		$attr['excerpt_length'] = ( isset($attr['excerpt_length']) && !empty($attr['excerpt_length']) ) ? absint($attr['excerpt_length']) : 20;
		$attr['hidden_featured_image'] = ( isset($attr['hidden_featured_image']) ) ? $attr['hidden_featured_image'] : false;
		$attr['hidden_category'] = ( isset($attr['hidden_category']) ) ? $attr['hidden_category'] : false;
		$attr['hidden_title'] = ( isset($attr['hidden_title']) ) ? $attr['hidden_title'] : false;
		$attr['hidden_date'] = ( isset($attr['hidden_date']) ) ? $attr['hidden_date'] : false;
		$attr['hidden_excerpt'] = ( isset($attr['hidden_excerpt']) ) ? $attr['hidden_excerpt'] : true;
		$attr['hidden_readmore'] = ( isset($attr['hidden_readmore']) ) ? $attr['hidden_readmore'] : true;
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		ob_start();
		
	?>
	<div class="libero_hottest_post">
		<div class="row">
			<div class="col-sm-4">
				<div class="libero_hottest_title text-center">
					<?php if( $attr['title'] ) { ?>
					<h3 class="libero_title"><?php echo $attr['title'] ?></h3>
					<?php } ?>
				</div>
			</div>
			<div class="col-sm-8">
				<!-- Owl -->
				<div class="owl-carousel libero_hottest_owl"  data-columns="<?php echo absint($attr['columns']) ?>">
					
					<?php 
						global $post;
						$args = libero_query_posts($attr);
						if( $attr['number_posts'] ) {
							$args['posts_per_page'] = $attr['number_posts'];
						}
						$the_query = new WP_Query( $args );
						if ( $the_query->have_posts() ) :
						while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
					
					<div class="libero_featured_post">
						
						<?php if( !$attr['hidden_featured_image'] && has_post_thumbnail() ) { ?>
						<div class="libero_featured_thumbnail">
							<?php the_post_thumbnail( 'libero-medium' ) ?>
							<div class="libero_thumbnail_overlay">
								<a class="libero_readmore" href="<?php the_permalink() ?>"><?php esc_html_e('Read more','libero') ?></a>
							</div>
						</div>
						<?php } ?>
						
						<div class="libero_featured_content">
						
							<div class="libero_content_meta">
								<?php if( ! $attr['hidden_category'] ) { ?>
								<span class="libero_category_name"><?php the_category(', '); ?></span>
								<?php } ?>
								
								<?php if( ! $attr['hidden_date'] ) { ?>
								<span>.</span>
								<?php libero_post_info(true,false,false) ?>
								<?php } ?>
							</div>
							
							<?php if( ! $attr['hidden_title'] ) { ?>
							<h5><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
							<?php } ?>
							
							<?php if( ! $attr['hidden_excerpt'] ) echo libero_get_excerpt_by_id($post->ID,$attr['excerpt_length']); ?>
							
							<?php if( ! $attr['hidden_readmore'] ) { ?>
							<a class="libero_continue_reading" href="<?php the_permalink() ?>"><?php esc_html_e('Continue Reading','libero') ?></a>
							<?php } ?>
							
						</div>
					</div>
					
					<?php 
						endwhile;
						endif;
					?>
					
				</div>
				<!-- .Owl -->
			</div>
			
		</div>
	</div>
	
	<?php
		wp_reset_postdata();
		wp_reset_query();
		return ob_get_clean();
	}
}
add_shortcode( 'libero_featured_posts_2', 'libero_vc_featured_posts_2' );

vc_map( array (
	'base' 			=> 'libero_featured_posts_2',
	'name' 			=> __('Slider Carousel Posts 2', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'categories',
			'type' 			=> 'terms',
			'heading' 		=> __('Category filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Multiple category filter.',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'tag_slug',
			'type' 			=> 'textfield',
			'heading' 		=> __('Filter by tag slug:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'To filter multiple tag slugs, enter here the tag slugs separated by commas (ex: tag1,tag2,tag3).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'authors_id',
			'type' 			=> 'textfield',
			'heading' 		=> __('Multiple authors filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Filter multiple authors by ID. Enter here the author IDs separated by commas (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'ids',
			'type' 			=> 'autocomplete',
			'heading' 		=> __('Include Posts:', 'mfn-opts'),
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
				'unique_values' => true,
			),
			'admin_label'	=> true,
			'description'	=> 'Filter by post ID (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'sort',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Sort order:', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
				"- Latest -" 	=> "",
				"Popular (all time)" 		=> "popular",
				"Popular (last 7 days)" 	=> "popular7",
				"Random Posts" 				=> "random_posts",
				"Random posts Today" 		=> "random_today",
				"Random posts from last 7 Day" 	=> "random_7_day",
				"Most Commented" 			=> "comment_count"
			),
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'number_posts',
			'type' 			=> 'textfield',
			'heading' 		=> __('Number Posts', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> 8
		),
		array (
			'param_name' 	=> 'columns',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Columns', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
				"4 Columns" => "4",
				"3 Columns" => "3",
				"2 Columns" => "2"
			),
			'std'			=> 3
		),
		array (
			'param_name' 	=> 'excerpt_length',
			'type' 			=> 'textfield',
			'heading' 		=> __('Excerpt Length', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '20'
		),
		array (
			'param_name' 	=> 'hidden_featured_image',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Featured Image', 'mfn-opts'),
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'hidden_category',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Category', 'mfn-opts'),
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'hidden_title',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Title', 'mfn-opts'),
			'admin_label'	=> true                                                     
		),
		array (
			'param_name' 	=> 'hidden_date',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Date', 'mfn-opts'),
			'admin_label'	=> true                                           
		),
		array (
			'param_name' 	=> 'hidden_excerpt',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Excerpt', 'mfn-opts'),
			'admin_label'	=> true,
			'std'			=> 'true'
		),
		array (
			'param_name' 	=> 'hidden_readmore',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Readmore', 'mfn-opts'),
			'admin_label'	=> true,
			'std'			=> 'true'
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));

//Filters For autocomplete param:
//For suggestion: vc_autocomplete_[shortcode_name]_[param_name]_callback
add_filter( 'vc_autocomplete_libero_featured_posts_2_ids_callback', 'postIdsAutocompleteSuggester', 10, 1 ); // Get suggestion(find). Must return an array
add_filter( 'vc_autocomplete_libero_featured_posts_2_ids_render', 'postIdsAutocompleteRender', 10, 1 ); // Render exact product. Must return an array (label,value)