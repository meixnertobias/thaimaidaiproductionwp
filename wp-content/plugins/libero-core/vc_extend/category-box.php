<?php
if( ! function_exists( 'lz_vc_category_box' ) ) {
	function lz_vc_category_box( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'image'							=> '',
			'term'							=> '',
			'animate'						=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['image'] = ( isset($attr['image']) && !empty($attr['image']) ) ? $attr['image'] : '';
		$attr['term'] = ( isset($attr['term']) && !empty($attr['term']) ) ? $attr['term'] : '';
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$class = array();
		$class[] = 'libero_one_category';
		if( $attr['animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		$class[] = $attr['extraclassname'];
		ob_start();
		?>
		<div class="<?php echo libero_class($class) ?>">
			<?php if( $attr['image'] ) { ?>
			<img src="<?php echo wp_get_attachment_url($attr['image']) ?>" alt="">
			<?php } ?>
			<?php 
				if( $attr['term'] ) { 
				$category = get_category_by_slug( $attr['term'] );
				if( $category ) {
			?>
			<a href="<?php echo get_category_link($category->term_id) ?>"><span><?php echo $category->name ?></span></a>
			<?php } } ?>
		</div>
		<?php
		return ob_get_clean();
	}
}
add_shortcode( 'lz_category_box', 'lz_vc_category_box' );

vc_map( array (
	'base' 			=> 'lz_category_box',
	'name' 			=> __('Category Box', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'image',
			'type' 			=> 'attach_image',
			'heading' 		=> __('Image', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array (
			'param_name' 	=> 'term',
			'type' 			=> 'term',
			'heading' 		=> __('Select Category', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
