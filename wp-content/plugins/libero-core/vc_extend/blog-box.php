<?php
if( ! function_exists( 'libero_vc_blog_box' ) ) {
	function libero_vc_blog_box( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'categories'					=> '',
			'tag_slug'						=> '',
			'authors_id'					=> '',
			'ids'							=> '',
			'sort'							=> '',
			'number'						=> '',
			'columns'						=> '',
			'pagenavi'						=> '',
			'hidden_category_top'			=> '',
			'is_hidden_thumbnail'			=> '',
			'is_hidden_category'			=> '',
			'is_hidden_date'				=> '',
			'is_hidden_comment'				=> '',
			'number_loadmore'				=> '',
			'excerpt_length'				=> '',
			'is_hidden_read_more'			=> '',
			'is_hidden_share'				=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = ( isset($attr['title']) && !empty($attr['title']) ) ? $attr['title'] : '';
		
		$attr['categories'] = ( isset($attr['categories']) && !empty($attr['categories']) ) ? $attr['categories'] : '';
		$attr['tag_slug'] = ( isset($attr['tag_slug']) && !empty($attr['tag_slug']) ) ? $attr['tag_slug'] : '';
		$attr['authors_id'] = ( isset($attr['authors_id']) && !empty($attr['authors_id']) ) ? $attr['authors_id'] : '';
		$attr['ids'] = ( isset($attr['ids']) && !empty($attr['ids']) ) ? $attr['ids'] : '';
		$attr['sort'] = ( isset($attr['sort']) && !empty($attr['sort']) ) ? $attr['sort'] : '';
		
		$attr['number'] = ( isset($attr['number']) && !empty($attr['number']) ) ? absint($attr['number']) : 10;
		$attr['columns'] = ( isset($attr['columns']) && !empty($attr['columns']) ) ? absint($attr['columns']) : 4;
		$attr['pagenavi'] = ( isset($attr['pagenavi']) && !empty($attr['pagenavi']) ) ? $attr['pagenavi'] : 0;
		$attr['hidden_category_top'] = ( isset($attr['hidden_category_top']) && !empty($attr['hidden_category_top']) ) ? $attr['hidden_category_top'] : false;
		$attr['is_hidden_thumbnail'] = ( isset($attr['is_hidden_thumbnail']) && !empty($attr['is_hidden_thumbnail']) ) ? $attr['is_hidden_thumbnail'] : false;
		$attr['is_hidden_category'] = ( isset($attr['is_hidden_category']) && !empty($attr['is_hidden_category']) ) ? $attr['is_hidden_category'] : false;
		$attr['is_hidden_date'] = ( isset($attr['is_hidden_date']) && !empty($attr['is_hidden_date']) ) ? $attr['is_hidden_date'] : false;
		$attr['is_hidden_comment'] = ( isset($attr['is_hidden_comment']) && !empty($attr['is_hidden_comment']) ) ? $attr['is_hidden_comment'] : false;
		$attr['is_hidden_excerpt'] = ( isset($attr['is_hidden_excerpt']) && !empty($attr['is_hidden_excerpt']) ) ? $attr['is_hidden_excerpt'] : false;
		$attr['is_hidden_read_more'] = ( isset($attr['is_hidden_read_more']) && !empty($attr['is_hidden_read_more']) ) ? $attr['is_hidden_read_more'] : false;
		$attr['is_hidden_share'] = ( isset($attr['is_hidden_share']) && !empty($attr['is_hidden_share']) ) ? $attr['is_hidden_share'] : false;
		$attr['number_loadmore'] = ( isset($attr['number_loadmore']) && !empty($attr['number_loadmore']) ) ? absint($attr['number_loadmore']) : 2;
		$attr['excerpt_length'] = ( isset($attr['excerpt_length']) && !empty($attr['excerpt_length']) ) ? absint($attr['excerpt_length']) : 30;
		$attr['extraclassname'] = ( isset($attr['extraclassname']) && !empty($attr['extraclassname']) ) ? $attr['extraclassname'] : '';
		
		$columns = 12/absint($attr['columns']);
		$class = array();
		$class[] = 'libero_blog_list';
		$class[] = 'libero_wrap_blog';
		
		
		if( $attr['extraclassname'] ) {
			$class[] = $attr['extraclassname'];
		}
		ob_start();
	?>
	
		<div class="<?php echo libero_class($class) ?>">
			
			<?php
				global $libero_attr, $libero_index;
				$libero_attr = $attr;
				$libero_attr['blog'] = true;
				$libero_query = libero_query_posts($libero_attr);
				if( $libero_attr['number'] ) {
					$libero_query['posts_per_page'] = $libero_attr['number'];
				}
				$liberokey = uniqid();
				$the_query = new WP_Query( $libero_query );
			?>
			<div class="libero_title_with_breadcrumb">
				
				<?php
					if( ! $attr['hidden_category_top'] ) libero_render_categories($libero_attr, $liberokey, false);
				?>
				<?php if($attr['title']) { ?>
				<h4 class="libero_title"><?php echo $attr['title'] ?></h4>
				<?php } ?>
			
			</div>
			
			<?php libero_render_script_ajax_block('content-box',$libero_attr, $liberokey); ?>
			
			<div class="libero-wrap-ajax">
				<div class="row libero-inner-ajax">
					<?php 
						if ( $the_query->have_posts() ) :
							$libero_index = 0;
							while ( $the_query->have_posts() ) : $the_query->the_post();
								$libero_index++;
								echo '<div class="col-sm-'. absint($columns) .'">';
									libero_get_template_part('content','box');
								echo '</div>';
								if( $libero_index % $attr['columns'] == 0 ) echo '<div class="clear"></div>';
								
							endwhile;
						endif;
					?>
				</div>
				<?php
					if( isset( $attr['pagenavi'] ) && $attr['pagenavi'] != '0' ) libero_paging_nav($the_query,$libero_attr,$liberokey);
				?>
			</div>
			
		</div>
	<?php
		wp_reset_postdata();
		wp_reset_query();
		return ob_get_clean();
	}
}
add_shortcode( 'libero_blog_box', 'libero_vc_blog_box' );

vc_map( array (
	'base' 			=> 'libero_blog_box',
	'name' 			=> __('Blog Grid', 'mfn-opts'),
	'category' 		=> __('Libero Shortcode', 'mfn-opts'),
	'icon' 			=> 'cm_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> ''
		),
		array (
			'param_name' 	=> 'categories',
			'type' 			=> 'terms',
			'heading' 		=> __('Category filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Multiple category filter.',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'tag_slug',
			'type' 			=> 'textfield',
			'heading' 		=> __('Filter by tag slug:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'To filter multiple tag slugs, enter here the tag slugs separated by commas (ex: tag1,tag2,tag3).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'authors_id',
			'type' 			=> 'textfield',
			'heading' 		=> __('Multiple authors filter:', 'mfn-opts'),
			'admin_label'	=> true,
			'description'	=> 'Filter multiple authors by ID. Enter here the author IDs separated by commas (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'ids',
			'type' 			=> 'autocomplete',
			'heading' 		=> __('Include Posts:', 'mfn-opts'),
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
				'unique_values' => true,
			),
			'admin_label'	=> true,
			'description'	=> 'Filter by post ID (ex: 13,23,18).',
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'sort',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Sort order:', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
				"- Latest -" 	=> "",
				"Popular (all time)" 		=> "popular",
				"Popular (last 7 days)" 	=> "popular7",
				"Random Posts" 				=> "random_posts",
				"Random posts Today" 		=> "random_today",
				"Random posts from last 7 Day" 	=> "random_7_day",
				"Most Commented" 			=> "comment_count"
			),
			'group'			=> 'Filters'
		),
		array (
			'param_name' 	=> 'number',
			'type' 			=> 'textfield',
			'heading' 		=> __('Limit post number', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '10'
		),
		array (
			'param_name' 	=> 'columns',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Columns', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> array(
				"4 Columns" 	=> "4",
				"3 Columns" 	=> "3",
				"2 Columns" 	=> "2"
			)
		),
		array (
			'param_name' 	=> 'pagenavi',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Show Navi', 'mfn-opts'),
			'value'			=> array(
									'Hidden' => '0',
									'Number' => 'number',
									'Load More' => 'load_more',
									'Next/Prev' => 'next_prev',
								),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'number_loadmore',
			'type' 			=> 'textfield',
			'heading' 		=> __('Number post loadmore', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '2',
			'dependency' => array(
				'element' => 'pagenavi',
				'value' => 'load_more',
			)
		),
		array (
			'param_name' 	=> 'hidden_category_top',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Categories Top Title', 'mfn-opts'),
			'admin_label'	=> true
		),
		array (
			'param_name' 	=> 'excerpt_length',
			'type' 			=> 'textfield',
			'heading' 		=> __('Excerpt Length', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '30'
		),
		array (
			'param_name' 	=> 'is_hidden_thumbnail',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Thumbnail', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'is_hidden_category',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Category', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'is_hidden_date',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Date', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'is_hidden_comment',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Comment Count', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'is_hidden_excerpt',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Excerpt', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'is_hidden_read_more',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Read More', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'is_hidden_share',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Hidden Share', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));

//Filters For autocomplete param:
//For suggestion: vc_autocomplete_[shortcode_name]_[param_name]_callback
add_filter( 'vc_autocomplete_libero_blog_box_ids_callback', 'postIdsAutocompleteSuggester', 10, 1 ); // Get suggestion(find). Must return an array
add_filter( 'vc_autocomplete_libero_blog_box_ids_render', 'postIdsAutocompleteRender', 10, 1 ); // Render exact product. Must return an array (label,value)