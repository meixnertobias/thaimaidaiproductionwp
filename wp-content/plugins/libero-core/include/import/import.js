jQuery(function($){
	$('form#rst_list_demo .list-image-demo a').click(function(e){
		e.preventDefault();
		$import_true = confirm('Are you sure to import dummy content? It will overwrite the existing data.');
		if($import_true == false) return;
		
		var demo = $(this).data('demo');
		var data = {'action': 'rs_import','data' : demo };
		
		jQuery('.rst-import .import_message').html('<i class="spinner" style="float: none; margin: 0px; vertical-align: middle;visibility: visible;"></i> Data is being imported please be patient, while the awesomeness is being created :)');

		$('html,body').animate({ scrollTop: $('.rst-import .import_message').offset().top }, 500);

		jQuery.post(ajaxurl, data, function(response) {
			jQuery('.rst-import .import_message').html(response);
		});
		
	});
})