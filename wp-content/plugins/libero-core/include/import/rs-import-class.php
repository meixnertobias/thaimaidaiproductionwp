<?php
class rst_import extends WP_Import {
	
	// Import Customize
    function import_customize( $file ) {
		// File exists?
		if ( ! file_exists( $file ) ) {
			wp_die(
				esc_html__( 'Import file could not be found. Please try again.', 'clothmart' ),
				'',
				array( 'back_link' => true )
			);
		}
		
		$data = file_get_contents( $file );
		$data = json_decode( $data,true );
		if( $data ) {
			foreach( $data as $name=>$customize ) {
				set_theme_mod( $name, $customize );
			}
		}
    }
	
	// Import RevSlider
	function import_rev_slider( $file ) {
		require_once(ABSPATH .'wp-content/plugins/revslider/revslider.php');
		// File exists?
		if ( ! file_exists( $file ) ) {
			wp_die(
				esc_html__( 'Import file could not be found. Please try again.', 'clothmart' ),
				'',
				array( 'back_link' => true )
			);
		}
		if( class_exists( 'RevSlider' ) ) {
			$_FILES['import_file']['tmp_name']= $file;
			$slider = new RevSlider();
			$slider->importSliderFromPost();
		}
	}
	
	// Import Menu	
	function import_menu( $file ) {
		// File exists?
		if ( ! file_exists( $file ) ) {
			wp_die(
				esc_html__( 'Import file could not be found. Please try again.', 'clothmart' ),
				'',
				array( 'back_link' => true )
			);
		}
		$data = file_get_contents( $file );
		$menus = json_decode( $data,true );
		$array_menus = array();
		if( $menus ) {
			foreach( $menus as $name=>$menu ) {
				$menu_object = get_term_by( 'slug', $menu, 'nav_menu' );
				if( $menu_object ) $array_menus[$name] = $menu_object->term_id;
			}
		}
		set_theme_mod('nav_menu_locations',$array_menus);
	}
	
	// Import Customize
    function import_options( $file ) {
		// File exists?
		if ( ! file_exists( $file ) ) {
			wp_die(
				esc_html__( 'Import file could not be found. Please try again.', 'clothmart' ),
				'',
				array( 'back_link' => true )
			);
		}
		$file_contents = file_get_contents( $file );
		$import_data = json_decode( $file_contents, true );
		if( $import_data ) {
			foreach( $import_data['options'] as $option_name=>$value ){
				$option_value = maybe_unserialize( $import_data['options'][ $option_name ] );
				
				if ( in_array( $option_name, $import_data['no_autoload'] ) ) {
					delete_option( $option_name );
					add_option( $option_name, $option_value, '', 'no' );
				} else {
					update_option( $option_name, $option_value );
				}
			}
			echo '<p>' . esc_html__( 'All done. ', 'clothmart' ) . ' <a href="' . admin_url() . '">' . esc_html__( 'Have fun!', 'clothmart' ) . '</a>' . '</p>';
		}
    }
	
	// Import Color Woocommerce
    function import_color( $file ) {
		// File exists?
		if ( ! file_exists( $file ) ) {
			wp_die(
				esc_html__( 'Import file could not be found. Please try again.', 'clothmart' ),
				'',
				array( 'back_link' => true )
			);
		}
		$file_contents = file_get_contents( $file );
		$import_data = json_decode( $file_contents, true );
		if( $import_data ) {
			foreach( $import_data as $name=>$value ) {
				$category = get_term_by('slug', $name, 'pa_color');
				$option_name = 'rs-taxonomy_'. $category->term_id .'_rs_metas';
				if( $category ) {
					$option_value = array(
						'rst_swatch' => 'color',
						'rst_swatch_color' => $value
					);
					update_option( $option_name, $option_value );
				}
			}
			echo '<p>' . esc_html__( 'All done. ', 'clothmart' ) . ' <a href="' . admin_url() . '">' . esc_html__( 'Have fun!', 'clothmart' ) . '</a>' . '</p>';
		}
    }
}
