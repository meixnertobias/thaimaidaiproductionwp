<?php
if( !function_exists( 'rst_import_callback' ) ) {
	function rst_import_callback() {
		global $wpdb;
		
		if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

		// Load Importer API
		require_once ABSPATH . 'wp-admin/includes/import.php';
		
		require LIBERO_PLUGIN_DIR . "/include/import/import-widget.php";

		if ( ! class_exists( 'WP_Importer' ) ) {
			$class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
			if ( file_exists( $class_wp_importer ) ) {
				require $class_wp_importer;
			}
		}

		if ( ! class_exists( 'WP_Import' ) ) {
			$class_wp_importer = LIBERO_PLUGIN_DIR . "/include/import/wordpress-importer.php";
			if ( file_exists( $class_wp_importer ) )
				require $class_wp_importer;
		}

		// Registe class rs_import
		require LIBERO_PLUGIN_DIR . "/include/import/rs-import-class.php";
		
		if ( class_exists( 'WP_Import' ) ) {
			
			$data_demo = $_REQUEST['data'];
			
			if( !isset( $data_demo ) || $data_demo == '' ) return;
			
			// Setup Menu
			// $menus = get_nav_menu_locations();
			// $array_menus = array();
			// foreach( $menus as $name=>$menu ) {
				// $menu_object = get_term( $menu, 'nav_menu' );
				// $array_menus[$name] = $menu_object->slug;
			// }
			// echo( json_encode($array_menus) );exit;
			
			
			$wp_import = new rst_import();
			$wp_import->fetch_attachments = true;
			
			if( get_post(1)->post_title == 'Hello world!' && get_post(2)->post_title == 'Sample Page' ) {
				wp_delete_post(1, true);
				wp_delete_post(2, true);
				wp_delete_post(3, true);
				wp_delete_post(4, true);
				wp_delete_post(5, true);
			}
			
			// Import Data
			$wp_import->import( LIBERO_PLUGIN_DIR . "/include/import/data/libero.data.$data_demo.xml" );
			
			// Import Customize
			// var_dump( json_encode(get_theme_mods()) );exit;
			$wp_import->rs_import_customize( LIBERO_PLUGIN_DIR . "/include/import/data/libero.customize.$data_demo.txt" );
			remove_theme_mod( 'nav_menu_locations' );
			
			// Setup Menu Location
			// foreach( get_theme_mod('nav_menu_locations') as $key=>$menu ){
				// $menus[$key] = get_term_by( 'id', $menu, 'nav_menu')->slug;
			// }
			// var_dump(json_encode($menus));exit;
			$wp_import->rs_import_menu( LIBERO_PLUGIN_DIR . "/include/import/data/libero.menus.$data_demo.txt" );
			
			// Setup Page Home
			update_option('show_on_front','page');
			$args = array(
				'posts_per_page'   => -1,
				'post_type'        => 'page',
			);
			$posts_array = get_posts( $args );
			foreach($posts_array as $item) {
				if( $data_demo == 'magazine' && $item->post_name == 'magazine-right-sidebar' ) {
					update_option('page_on_front',$item->ID);
				}
				if( ( $data_demo == 'blog' || $data_demo == 'blog2' ) && $item->post_name == 'blog-classic-right-sidebar' ) {
					update_option('page_on_front',$item->ID);
				}
			}

			// Setup Widgets
			$file_widget = LIBERO_PLUGIN_DIR . "/include/import/data/libero.widgets.$data_demo.wie" ;
			wie_process_import_file( $file_widget );
			
			echo 'All done!';
		
		}
		die(); // this is required to return a proper result
	}
}
function rst_admin_import_scripts() {
    wp_register_script( 'rst-import', LIBERO_PLUGIN_URL . '/include/import/import.js', false, '1.0.0' );
    wp_enqueue_script( 'rst-import' );
}
add_action( 'admin_enqueue_scripts', 'rst_admin_import_scripts' );
add_action( 'wp_ajax_rs_import', 'rst_import_callback' );


