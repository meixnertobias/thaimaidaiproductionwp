<?php
	require LIBERO_PLUGIN_DIR . '/include/import/rs-import.php';
	
	add_action('admin_menu', 'rst_register_menu_page');

	function rst_register_menu_page() {
		 add_theme_page( 'Import Demo Data', 'Import Data', 'edit_theme_options', 'rst-import-data', 'rst_custom_menu_page_import' );
	}

	function rst_custom_menu_page_import(){
	?>
		<style type="text/css">
			.rst-import .list-image-demo a { width: 30%; height: auto; display: inline-block; vertical-align: top; margin-right: 3%;text-align: center; }
			.rst-import .list-image-demo a img { max-width: 100%; height: auto; }
		</style>
		<div id="poststuff" class="rst-import">
			<div class="postbox  hide-if-js" id="postexcerpt" style="display: block;">
				<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle"><span>Import Demo Data</span></h3>
				<div class="inside">
					
					<p>You must import the sample data file before customizing your theme.
					If you customize your theme, and later import a sample data file, all current contents entered in your site will be overwritten to the default settings of the file you are uploading! Please proceed with the utmost care, after exporting all current data! </p>
					<p><u><b>Note</b></u>: If you get errors, please be sure that your server configured Memory Limit &gt;=64MB and Execution Time &gt;=60.</p>
					<br />
					<form action="" id="rst_list_demo">
						<table class="form-table">
							<tr>
								<th class="row">Demo Source</th>
								<td class="list-image-demo">
									<a href="#" data-demo="magazine"><h3>Demo Magazine</h3><img src="<?php echo LIBERO_PLUGIN_URL . '/include/import/images/magazine.jpg' ?>" alt="" /></a>
									<a href="#" data-demo="blog"><h3>Demo Blog</h3><img src="<?php echo LIBERO_PLUGIN_URL . '/include/import/images/blog.jpg' ?>" alt="" /></a>
									<a href="#" data-demo="blog2"><h3>Demo Blog 2</h3><img src="<?php echo LIBERO_PLUGIN_URL . '/include/import/images/blog-2.jpg' ?>" alt="" /></a>
								</td>
							</tr>
						</table>
					</form>
					
				</div>
				<div class="inside">
					<p class="import-message">
						<span class="import_message"></span>
						<br style="clear:both" />
					</p>
				</div>
			</div>
		</div>
	<?php
	}
?>