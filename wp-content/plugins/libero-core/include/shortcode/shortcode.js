(function() {
    tinymce.create('tinymce.plugins.WPTuts', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {
			
            ed.addButton('libero_dropcap', {
				type: 'listbox',
				values: [
					{text: 'Dropcap Style', value: ''},
					{text: 'Style 1', value: '1'},
					{text: 'Style 2', value: '2'},
					{text: 'Style 3', value: '3'},
					{text: 'Style 4', value: '4'},
					{text: 'Style 5', value: '5'},
					{text: 'Style 6', value: '6'},
					{text: 'Style 7', value: '7'},
					{text: 'Style 8', value: '8'},
					{text: 'Style 9', value: '9'},
					{text: 'Style 10', value: '10'},
					{text: 'Style 11', value: '11'},
					{text: 'Style 12', value: '12'},
				],
				onselect: function(e) {
					value = this.value();
					before = '<span class="libero-dropcap libero-dropcap-style-' + value + '">';
					after = '</span>';
					tinyMCE.activeEditor.selection.setContent( before + tinyMCE.activeEditor.selection.getContent() + after );
				},
                title : 'Add dropcap',
                cmd : 'libero_dropcap'
            });

        },

        /**
         * Creates control instances based in the incomming name. This method is normally not
         * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
         * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
         * method can be used to create those.
         *
         * @param {String} n Name of the control to create.
         * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
         * @return {tinymce.ui.Control} New control instance or null if no control was created.
         */
        createControl : function(n, cm) {
            return null;
        },

        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo : function() {
            return {
                    longname : 'Redsand Short Code',
                    author : 'Thanh Cao',
                    authorurl : 'http://wp.tutsplus.com/author/leepham',
                    infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
                    version : "0.1"
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add('wptuts', tinymce.plugins.WPTuts);
	
})();