<?php
add_action( 'init', 'libero_wptuts_buttons' );
function libero_wptuts_buttons() {
	add_filter("mce_external_plugins", "libero_wptuts_add_buttons");
    add_filter('mce_buttons', 'libero_wptuts_register_buttons');
}
function libero_wptuts_add_buttons($plugin_array) {
	$plugin_array['wptuts'] = LIBERO_PLUGIN_URL . '/include/shortcode/shortcode.js';
	return $plugin_array;
}
function libero_wptuts_register_buttons($buttons) {
	array_push( $buttons,'libero_dropcap');
	return $buttons;
}