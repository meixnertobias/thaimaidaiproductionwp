<?php

function libero_get_template_part( $slug, $name = '' ) {
    $template = '';
	$template_path = "template-parts/";
	$plugin_path = LIBERO_PLUGIN_DIR . "/templates/";

    // Look in yourtheme/slug-name.php and yourtheme/template-parts/slug-name.php
    if ( $name ) {
        $template = locate_template( array( "{$slug}-{$name}.php", $template_path . "{$slug}-{$name}.php" ) );
    }

    // Get default slug-name.php
    if ( ! $template && $name && file_exists( $plugin_path . "{$slug}-{$name}.php" ) ) {
        $template = $plugin_path . "{$slug}-{$name}.php";
    }

    // If template file doesn't exist, look in yourtheme/slug.php and yourtheme/template-parts/slug.php
    if ( ! $template ) {
        $template = locate_template( array( "{$slug}.php", $template_path . "{$slug}.php" ) );
    }
	
	 // Get default slug.php
	if ( ! $template && file_exists( $plugin_path . "{$slug}.php" ) ) {
        $template = $plugin_path . "{$slug}.php";
    }
	
    if ( $template ) {
        load_template( $template, false );
    }
}

function libero_get_categories_ids($slug) {
	$str = '';
	$term = array();
	$array_slug = explode(',',$slug);
	if( is_array($array_slug) ) {
		foreach($array_slug as $item) {
			if( is_object(get_term_by('slug', $item, 'category')) ) {
				$term[] = get_term_by('slug', $item, 'category')->term_id;
			}
		}
	}
	return implode(",",$term);
}

function libero_render_script_ajax_block($template, $libero_attr, $liberokey) {
	ob_start();
	?>
	<script type="text/javascript">
		var libero_<?php echo esc_html($liberokey) ?> = new libero_blocks();
		libero_<?php echo esc_html($liberokey) ?>.atts = '<?php echo json_encode($libero_attr) ?>';
		libero_<?php echo esc_html($liberokey) ?>.template = '<?php echo $template ?>'; 
	</script>
	<?php
	echo ob_get_clean();
}

function libero_render_categories($libero_attr, $liberokey, $is_ajax = true) {
	ob_start();
	$class_ajax = '';
	if( $is_ajax ) $class_ajax = 'class="libero_subcat_ajax"';
	if( isset($libero_attr['categories']) && !empty($libero_attr['categories']) && libero_get_categories_ids($libero_attr['categories']) ) {
		$categories = get_categories( array('include' => libero_get_categories_ids($libero_attr['categories']), 'hide_empty' => 1, 'orderby' => 'term_group' ) );
		if( sizeof($categories) == 1 ) {
			$categories = get_categories( array('parent' => libero_get_categories_ids($libero_attr['categories']), 'hide_empty' => 1, 'orderby' => 'term_group' ) );
		}
		if( sizeof($categories) > 1 ) {
	?>
	<ul class="breadcrumb">
		<li class="active"><a <?php echo $class_ajax ?> href="#"><?php esc_html_e('All','libero') ?></a></li>
		<?php foreach( $categories as $category ) { ?>
		<li><a <?php echo $class_ajax ?> href="<?php echo esc_url(get_category_link( $category->term_id )) ?>"  data-key="libero_<?php echo esc_html($liberokey) ?>" data-term="<?php echo esc_attr($category->slug) ?>"><?php echo $category->name ?></a></li>
		<?php } ?>
	</ul>
	<?php
		}
	}
	echo ob_get_clean();
}

