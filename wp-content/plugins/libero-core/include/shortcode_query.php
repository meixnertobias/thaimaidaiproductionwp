<?php

/*
*	Query shortcode
*
*/


function libero_query_posts( $attr ) {
	
	$categories = isset($attr['categories']) ? $attr['categories'] : '';
	$tags = isset($attr['tag_slug']) ? $attr['tag_slug'] : '';
	$authors_id = isset($attr['authors_id']) ? $attr['authors_id'] : '';
	$ids = isset($attr['ids']) ? $attr['ids'] : '';
	$sort = isset($attr['sort']) ? $attr['sort'] : '';
	
	$wp_query_args = array(
		'ignore_sticky_posts' => 1,
		'post_status' => 'publish',
		'post_type'		=> 'post'
	);
	
	if( isset($attr['blog']) && $attr['blog'] ) {
		$wp_query_args['paged'] = max( get_query_var('paged'), get_query_var('page'), 1);
	}
	if ( !empty($categories) ) {
		$wp_query_args['cat'] = libero_get_categories_ids($categories);
	}
	if ( !empty($tags) ) {
		$wp_query_args['tag'] = str_replace(' ', '-', $tags);
	}
	if (!empty($authors_id)) {
		$wp_query_args['author'] = $authors_id;
	}
	switch ($sort) {
		case 'popular':
			$wp_query_args['meta_key'] = 'libero_post_views_count';
			$wp_query_args['orderby'] = 'meta_value_num';
			$wp_query_args['order'] = 'DESC';
			break;
		case 'popular7':
			$wp_query_args['meta_key'] = 'libero_post_views_count';
			$wp_query_args['orderby'] = 'meta_value_num';
			$wp_query_args['order'] = 'DESC';
			$wp_query_args['date_query'] = array(
				'column' => 'post_date_gmt',
				'after' => '1 week ago'
			);
			break;
		case 'random_posts':
			$wp_query_args['orderby'] = 'rand';
			break;
		case 'comment_count':
			$wp_query_args['orderby'] = 'comment_count';
			$wp_query_args['order'] = 'DESC';
			break;
		case 'random_today':
			$wp_query_args['orderby'] = 'rand';
			$wp_query_args['year'] = date('Y');
			$wp_query_args['monthnum'] = date('n');
			$wp_query_args['day'] = date('j');
			break;
		case 'random_7_day':
			$wp_query_args['orderby'] = 'rand';
			$wp_query_args['date_query'] = array(
				'column' => 'post_date_gmt',
				'after' => '1 week ago'
			);
			break;
	}
	if (!empty($ids)) {
		$wp_query_args['post__in'] = explode(',',$ids);
		$wp_query_args['orderby'] = 'post__in';
	}
	return $wp_query_args;
}
