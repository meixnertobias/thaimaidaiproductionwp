<?php
add_action('wp_ajax_libero_ajax_blog', 'libero_ajax_blog_action');
add_action('wp_ajax_nopriv_libero_ajax_blog', 'libero_ajax_blog_action');

function libero_ajax_blog_action() {
	
	$atts = $_POST['atts'];
	$paged = $_POST['paged'];
	$template = $_POST['template'];
	$atts = str_replace('\"', '"', $atts);
	$atts = json_decode($atts,true);
	
	$libero_query = libero_query_posts($atts);
	$libero_query['posts_per_page'] = $atts['number_loadmore'];
	$libero_query['paged'] = 1;
	$libero_query['offset'] = $atts['number'] + $atts['number_loadmore']*($paged-2);
	$the_query = new WP_Query( $libero_query );
	
	ob_start();
	switch ($template) {
		case 'content-large-grid':
		case 'content-box':
			$columns = 12/absint($atts['columns']);
			$libero_index = $libero_query['offset'];
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
					echo '<div class="col-sm-'. absint($columns) .'">';
						libero_get_template_part('content','box');
					echo '</div>';
					$libero_index++;
					if( $libero_index % $atts['columns'] == 0 ) echo '<div class="clear"></div>';
				endwhile;
			endif;
			break;
		case 'content':
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
					libero_get_template_part('content');
				endwhile;
			endif;
			break;
		case 'content-medium':
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();
					libero_get_template_part('content','medium');
				endwhile;
			endif;
			break;
	}
	wp_reset_postdata();
	wp_reset_query();
	echo ob_get_clean();
	exit;
}


add_action('wp_ajax_libero_ajax_horizontal', 'libero_ajax_horizontal_action');
add_action('wp_ajax_nopriv_libero_ajax_horizontal', 'libero_ajax_horizontal_action');

function libero_ajax_horizontal_action() {
	global $libero_attr;
	$atts = $_POST['atts'];
	$term = $_POST['term'];
	$template = $_POST['template'];
	$atts = str_replace('\"', '"', $atts);
	$atts = json_decode($atts,true);
	$libero_attr = $atts;
	
	if( $term ) {
		$libero_attr['categories'] = $term;
	}
	
	ob_start();
	libero_get_template_part( $template );
	echo ob_get_clean();
	exit;
}