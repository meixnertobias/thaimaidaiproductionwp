<?php
add_action( 'init', 'libero_create_brand_taxonomies', 9999 );

function libero_create_brand_taxonomies() {
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Brand', 'taxonomy general name' ),
    'singular_name' => _x( 'brand-category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Genres' ),
    'all_items' => __( 'All Brands' ),
    'parent_item' => __( 'Parent brand' ),
    'parent_item_colon' => __( 'Parent brand:' ),
    'edit_item' => __( 'Edit category' ), 
    'update_item' => __( 'Update category' ),
    'add_new_item' => __( 'Add New brand' ),
    'new_item_name' => __( 'New category Name' ),
    'menu_name' => __( 'Brand' ),
  ); 	

  register_taxonomy('libero-brand',array('product'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'brand' ),
  ));
}