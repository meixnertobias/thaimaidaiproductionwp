<div class="wrap" ng-app="app">
    <div id="icon-options-general" class="icon32"><br/></div>
    <h2><?php esc_html_e(WPAI_NAME); ?></h2>
    
     <div ng-controller="AppController as vm" ng-cloak>
     	<uib-tabset>
     	
     		<uib-tab select="goState('blocks')" active="tabs['blocks']"
     			uib-tooltip="<?php echo __('Define here different ad blocks by pasting adsense code. These blocks can then be placed at different locations on your site.','wpailang');?>"
     			tooltip-placement="right"> 
      			<tab-heading>
        			<i class="glyphicon glyphicon-euro"></i> 
        			<?php echo __('Blocks','wpailang');?>
      			</tab-heading>
 			     <div class="content" ui-view="blocks"></div>
    		</uib-tab>
    		
    		<uib-tab select="goState('placements')" active="tabs['placements']"
    			tooltip-placement="right"
    			uib-tooltip="<?php echo __('Select for each location which ad block you would like to see displayed','wpailang');?>">
    			<tab-heading>
        			<i class="glyphicon glyphicon-th-large"></i><?php echo __('Placements','wpailang');?>
      			</tab-heading>
    			 <div class="content" ui-view="placements"></div>
    		</uib-tab>
    		
    		<uib-tab select="goState('settings')" active="tabs['settings']"    			
    			tooltip-placement="right"
    			uib-tooltip="<?php echo __('Set options influencing how the ads are displayed.','wpailang');?>">
      			<tab-heading>
        			<i class="glyphicon glyphicon-cog"></i>
        			<?php echo __('Settings','wpailang');?>
      			</tab-heading>
				<div class="content" ui-view="settings"></div>
    		</uib-tab>
    		
    		<uib-tab select="goState('info')" active="tabs['info']"
    			tooltip-placement="right"
    			uib-tooltip="<?php echo __('You should know this','wpailang');?>">
      			<tab-heading>
        			<i class="glyphicon glyphicon-eye-open"></i>
        			<?php echo __('Hints','wpailang');?>
      			</tab-heading>
 			     <div class="content" ui-view="info"></div>
    		</uib-tab>
    		
    		<li id="tab-btn-container" class="btn btn-primary" >
	    		<uib-tab ng-repeat="tab in mtabs" ng-click="tabs[tab.targetTab]=true;vm.createObj();" >    		
	    			<tab-heading>
	        			<i class="glyphicon glyphicon-plus"></i>
	        			{{tab.title}}
	      			</tab-heading>
	    		</uib-tab>
    		</li>
    		
    		<li id="tab-progress-container" class="btn" >
	    		<img src="<?php echo WPAI_PLUGIN_URL . '/images/loading.gif';?>" ng-hide="progress &lt; 1" ng-show="progress &gt; 0"/>
    		</li>
  		</uib-tabset>
     </div>
	 <div id="wpai-admin-footer"><?php echo __('Do you like our <strong>WP Advertize It</strong> plugin? If yes - give us your ','wpailang');?><a href="https://wordpress.org/support/view/plugin-reviews/wp-advertize-it?filter=5#postform" target="_blank" class="wpai-rating-link">&#9733;&#9733;&#9733;&#9733;&#9733;</a> <?php echo __('rating! Thank you!','wpailang');?> </div>
</div>
