function PlacementController($rootScope, $scope,  PlacementService, BlockService) {
    var vm = this;
    
    $scope.placements = PlacementService.model; 
    $scope.masterPlacements = PlacementService.masterModel;
    $scope.placementStates = {};
    $scope.placementStates.state = PlacementService.placementStates;
    
    $scope.allblocks = BlockService.model;
    
    //TBD: focus by sorting issue... wtf
    //$scope.selectedPlacemntId = 10;
    
    //$rootScope.mtabs = [{ targetTab:'placements', title:'Add Placement', content:'Dynamic content 1' }];
    $rootScope.mtabs = [];
    
    $scope.tabs = {'blocks':false, 'placement':true, 'settings':false};
    
    PlacementService.getPlacements().then(function(){    	
    	angular.forEach($scope.placements.data, function(value) {
    		
    		PlacementService.placementStates[value.id] = {'changed':false,'opened':false};
    		
    		$scope.$watch("placements.data['"+value.id+"'] | json", function(newValue, oldValue){
    			if (newValue){ 
    				var obj = $scope.placements.data[value.id];
	    			if (JSON.stringify($scope.masterPlacements.data[value.id]) != JSON.stringify(obj)){
	    				PlacementService.placementStates[obj.id].changed = true;
	    			}else{
	    				PlacementService.placementStates[obj.id].changed = false;
	    			}
    			}
    			
    		});
    		
    		//for new elements
    		$scope.$watchCollection(function() {return PlacementService.placementStates;}, function(newcol, oldcol){
    			$scope.$watch("placements.data["+(newcol.length-1)+"] | json", function(newValue, oldValue){ 
    				if (newValue){ 
	        			var obj = angular.copy($scope.placements.data[(newcol.length-1)]);
	        			delete obj.$$hashKey;
	        			if (JSON.stringify($scope.masterPlacements.data[obj.id]) != JSON.stringify(obj)){
	        				PlacementService.placementStates[obj.id].changed = true;
	        			}else{
	        				PlacementService.placementStates[obj.id].changed = false;
	        			}
    				}
    			});        		
        	});
    		
    	});
    });
    
    $scope.update = function(placement) {
    	if (placement){
    		$scope.masterPlacements.data[placement.id] = angular.copy($scope.placements.data[placement.id]);
    	}
    	
    	PlacementService.savePlacement(placement);    	
    };

    $scope.reset = function(placement) {
    	if (placement){
    		$scope.placements.data[placement.id] = angular.copy($scope.masterPlacements.data[placement.id]);
    	}
    };
    
    $scope.delete = function(placement) {
    	if (placement){
    		delete($scope.masterPlacements.data[placement.id]);
    		delete($scope.placements.data[placement.id]); 
    		
    		PlacementService.deletePlacement(placement);
    	}
    };
    
    $scope.selectBlock = function(placement, block){
    	if (block){
    		placement.blockid = block.id;
    	}else{
    		placement.blockid = 0;
    	}
    }
}

angular
    .module('app')
    .controller('PlacementController', PlacementController);