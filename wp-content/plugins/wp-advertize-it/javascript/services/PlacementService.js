
function PlacementService( $http, HttpResponseService, $filter,$rootScope) {

	var vm = this;
	
	this.model = {};
	this.masterModel = {};
	this.placementStates = {};
	
	this.placementStates[0] = {'changed':true,'opened':true};
	
    this.getPlacements = function() {
    	    	
		var data = 'action=get_placements&nt='+new Date().getTime();
		
        var promise = $http({
            method : 'POST',
            url : ajaxurl,
            data:  data,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}  

        });
        
        return HttpResponseService.handle(promise, function(response) {        
                	vm.model.data = {};
                	vm.masterModel.data = {};
                	vm.placementStates.length = 0;
                	                	
                	angular.forEach(response.dbplacements, function(value, key) {
                		
                		vm.placementStates[value.id] = {'changed':false, 'opened':false};
                		
                		vm.model.data[value.id] = value;
                		vm.model.data[value.id].name_i10n = response.defaultplacements[value.name].txt;
                		vm.model.data[value.id].defval = response.defaultplacements[value.name].val;
                		vm.masterModel.data[value.id] = angular.copy(value);
                		
                	});               	
           });
 
    };
    
    this.createPlacement  = function() {
    	var tempid = 0;
    	vm.model.data[tempid] = {'id':tempid, 
    			'name':'new Placement'};
    	
    	vm.masterModel.data[tempid] = angular.copy(vm.model.data[tempid]);
    	
    	vm.placementStates[tempid] = {'changed':true, 'opened':true};
    	
    };
    
    this.savePlacement = function(placement) {
    	
    	var ajaxurl_post;
    	
    	if(placement.id==0){
    		delete(vm.masterModel.data[0]);
    		delete(vm.model.data[0]);    		
    		ajaxurl_post = ajaxurl+'?action=create_placement&nt='+new Date().getTime();
    	} else{
    		ajaxurl_post = ajaxurl+'?action=save_placement&nt='+new Date().getTime();
    	}
    	
    	
        var promise = $http({
            method : 'POST',
            url : ajaxurl_post,
            data:  { 'placement' :{
            	'blockid': placement.blockid, 
            	'id': placement.id, 
            	'name': placement.name,
            	'type': placement.type}} 
        });
        
        return HttpResponseService.handle(promise, function(data) {
                if (data) {
  
                	vm.placementStates[data.id] = {'changed':false,'opened':true};
                	
                	vm.model.data[data.id].blockid = data.blockid;//only blockid can be changed now
                	vm.masterModel.data[data.id] = angular.copy(vm.model.data[data.id]);
               }               
        });
	};
	
	this.deletePlacement = function(block){
		ajaxurl_post = ajaxurl+'?action=delete_placement&nt='+new Date().getTime();
	
	    var promise = $http({
	        method : 'POST',
	        url : ajaxurl_post,
	        data:  { 'placement' :placement.id } 
	    });
    
	    return HttpResponseService.handle(promise, function(data) {});
	};
   
    return {
    	getPlacements: this.getPlacements,
    	savePlacement: this.savePlacement,
    	createPlacement: this.createPlacement,
    	deletePlacement: this.deletePlacement,
    	model: this.model,
    	masterModel: this.masterModel,
    	placementStates: this.placementStates
    };
}

angular
    .module('app')
    .factory('PlacementService', PlacementService);